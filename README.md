# README #

Licensed by Mendel University.

### What is this repository for? ###

- Application using Genetic Algorithms and Simulated Annealing to solve well-known Set Covering Problem.
- Version: 1. 0

### How do I get set up? ###

* Clean and Build application and install artifact for jars DJNative-Swing.jar and DJNative-Swing-SWT.jar which are in resources folder.


### Pictures from application ###

**Genetic Algorithm cycle:**
![GA.png](https://bitbucket.org/repo/ojrnpo/images/211156302-GA.png)

**Simulated Annealing cycle:**
![SimulatedAnnealing3.png](https://bitbucket.org/repo/ojrnpo/images/3301993218-SimulatedAnnealing3.png)

**Show data using XLST transformation:**
![ViewDataInApp.png](https://bitbucket.org/repo/ojrnpo/images/2549014397-ViewDataInApp.png)

**Videl tutorial in application:**
![video.png](https://bitbucket.org/repo/ojrnpo/images/2422430752-video.png)

**Email frame:**

![email.png](https://bitbucket.org/repo/ojrnpo/images/3240160506-email.png)