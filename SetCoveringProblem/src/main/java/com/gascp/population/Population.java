package com.gascp.population;

import com.gascp.matrix.Matrix;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Seda
 */

public class Population implements Comparable<Individual> {
    
    private static int POPULATION_SIZE = 200;
    private List<Individual> population;
    private int totalFitness;
    private int bestFitness;
    private Matrix matrix;
    
    public Population(Matrix matrix) {
        this.matrix = matrix;
        totalFitness = 0;
        bestFitness = Integer.MAX_VALUE;
        population = new ArrayList<>();
        // inicializuje populaci chromosomy
        for(int i=0; i<POPULATION_SIZE;i++){
            population.add(new Individual(matrix));
            population.get(i).generateChromosome();
            population.get(i).setChromosomeCovering();
            population.get(i).addNecessaryRowsToNServices();
        }
                
        printPopulation();
    }
    
    // ohodnoti jedince v populaci a nastavi jim nejakou fitness
    public void evaluateIndividuals(){    
        for(int i = 0; i<POPULATION_SIZE;i++){
            this.population.get(i).evaluateIndividual();
            //System.out.println("Jedinec: " + (i+1) + " ma fitness hodnotu: " +  population[i].getFitnessValue());
            this.totalFitness += this.population.get(i).getFitnessValue();
        }
    }
    
    public double calculateAverageFitness(){
        int average = 0;
        int suma = 0;
        for(int i=0;i<POPULATION_SIZE;i++){
            suma = suma + this.population.get(i).getFitnessValue();
        }
        return suma/POPULATION_SIZE;       
    }
    
    public int findBestFitness(){
        int rank=0;
        int bestFitness = Integer.MAX_VALUE;
        for(int i = 0; i<POPULATION_SIZE;i++){
            if(this.population.get(i).getFitnessValue() < bestFitness){
                bestFitness = this.population.get(i).getFitnessValue();
            }
        }        
        return bestFitness;
    }
    
    public int calculateBestFitness(){
        return 0;      
    }
    
    public int getTotalFitnessValue(){
        return totalFitness;
    }
    
    public Individual findBestIndividual() {
        int rank = 0;
        for(int i = 0; i<this.POPULATION_SIZE;i++){
            if(this.population.get(i).getFitnessValue() < this.bestFitness){
                this.bestFitness = this.population.get(i).getFitnessValue();
                rank = i;
            }
        }
        return population.get(rank);
    }
    
    public int getBestFitness(){
        return bestFitness;
    }    
    
    /**
     * 
     * @return specific chromosome from population
     */
    public List<Individual> getPopulation(){
        return population;
    }

    public static int getPOPULATION_SIZE() {
        return POPULATION_SIZE;
    }

    public static void setPOPULATION_SIZE(int POPULATION_SIZE) {
        Population.POPULATION_SIZE = POPULATION_SIZE;
    }
    
    
    /**
     * prints population chromosomes (TRUE,FALSE) values
     */
    public void printPopulation(){
        //System.out.println();
        //System.out.println("Inicializovana populace je tvorena temito chromosomy: ");       
        for(int i=0; i<POPULATION_SIZE; i++){
            //System.out.println(Arrays.toString(population.get(i).getChromosome()));
        }        
    }

    public static int getPopulationSize(){
        return POPULATION_SIZE;
    }
    
    public void setMutationRate(double mutationRate){
        for(int i=0; i<POPULATION_SIZE;i++){
            population.get(i).setMutationrate(mutationRate);
        }
    }
    
    public static Comparator<Individual>  IndividualFitnessComparator = new Comparator<Individual>() {

        @Override
        public int compare(Individual t1, Individual t2) {
            Integer fitnessValue1 = t1.getFitnessValue();
            Integer fitnessValue2 = t2.getFitnessValue();

            //descending order
            return fitnessValue2.compareTo(fitnessValue1);

            //ascending order
            //return fitnessValue1.compareTo(fitnessValue2);
        }
    };

    @Override
    public int compareTo(Individual t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
