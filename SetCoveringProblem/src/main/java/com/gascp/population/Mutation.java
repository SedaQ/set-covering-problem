package com.gascp.population;

import com.gascp.population.Individual;

/**
 *
 * @author Seda
 */

public class Mutation {
    
    private Individual ind;
    private double ratio;

    public Mutation(){}
    
    /**
     * 
     * @param ind specific Individual which will be used for mutation
     */
    public Mutation(Individual ind){
        this.ind = ind;
    }     
    
    /**
     * mutate specific gen of specifi Individualthat means reverse value of boolean true to false, false to true
     */
    public void mutation() {
        double rnd = Individual.getRnd().nextDouble();
        if(rnd<=this.ratio){
            int index = Individual.getRnd().nextInt(ind.getMatrix().getUnNecessaryRows().size());
            this.ind.setGene(index, reverseBoolean(ind.getGene(index)));    // flip
        }
    }
    
    /**
     * 
     * @param booleanek specific gene reverse his value
     * @return reversed boolean
     */
    public boolean reverseBoolean(boolean booleanek){
        return booleanek ^= true;
    }
    
    public void setMutation(double ratio){
        this.ratio = ratio;
    }
    
    public double getMutationRate(){
        return ratio;
    }
    
}
