package com.gascp.population;

import com.gascp.matrix.IChromosomeRepair;
import com.gascp.matrix.IntegerComparator;
import com.gascp.matrix.Matrix;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Seda
 */
public class Individual implements IChromosomeRepair {
    
    private boolean[] chromosome;
    private static Random rnd = new Random();
    private List<Integer> unNecessaryRows;
    private List<Integer> necessaryRows;
    private int[] chromosomeCovering;
    private int[] originCovering;
    private List<Integer> necessaryServices;
    private int matrixColumnsLength;
    private int fitnessValue;
    private int surplusIndividual;
    private Mutation mutate;
    private Matrix matrix;
      
    /**
     * Constructor initialize some fields and runs methods for initialize chromosome and set origin chromosomeCovering (from unnecessary rows)
     * @param matrix
     */
    public Individual(Matrix matrix){
        this.matrix = matrix;
        fitnessValue = 0;
        surplusIndividual = matrix.getSurplus();
        mutate = new Mutation(this);
        chromosome = new boolean[matrix.getUnNecessaryRows().size()];
        chromosomeCovering = new int[matrix.getMatrixColumnsLength()];
        necessaryServices = new ArrayList<>();
        originCovering = matrix.getUNContainsColumns();
        unNecessaryRows = matrix.getUnNecessaryRows();
        necessaryRows = matrix.getNecessaryRows();
        matrixColumnsLength = matrix.getMatrixColumnsLength();
    }
    
    /**
     * saved into field - chromosomeCovering which customers this solution (chromosome) covers
     */
    public void setChromosomeCovering(){
        setOriginCovering();
        //int[] rows = new int[SIZE+necessaryRows.size()];
        int[] row = new int[this.matrixColumnsLength];
        for(int i=0; i<this.chromosome.length;i++){
            if(this.chromosome[i]==true){
                row = matrix.getRowFromMatrix(this.matrix.getMatrix(), this.unNecessaryRows.get(i));
                //System.out.println("Radek, kterej si beru z matice: " + unNecessaryRows.get(rank) + " : vypada takhle > "+ Arrays.toString(row) );
                for(int k=0;k<row.length;k++){
                    if(row[k]==1){
                        if(this.chromosomeCovering[k]==1){
                            this.surplusIndividual++;
                        } else{
                            this.chromosomeCovering[k]=1;
                        }
                    }
                }              
                // uklada pozice radku, ktere jsou nezbytne pro pokryti danych zakazniku
                this.necessaryServices.add(unNecessaryRows.get(i));
            }
        }

        
        // pokud nebyli timto resenim pokryti vsichni zakaznici, tak provadi opravu, do te doby nez se pokryji vsichni zakaznici
        if(!allAreTrue(this.chromosomeCovering)){
            repairOperator();
        }               
    }
    
    public void addNecessaryRowsToNServices(){
        for(Integer list : this.necessaryRows){
            this.necessaryServices.add(list);
        }        
    }
    
    
    /**
     * repairs chromosome if specific chromosome didn't cover all customers
     * it is repairing until the chromosome is valid ( it means until it
     */
    @Override
    public void repairOperator() {
        int[] row = new int[this.matrixColumnsLength];
        for(int i=0; i<this.chromosome.length;i++){
            if(this.chromosome[i] == false && !this.necessaryServices.contains(this.unNecessaryRows.get(i))){
                reverseBoolean(this.chromosome[i]);
                row = this.matrix.getRowFromMatrix(this.matrix.getMatrix(), this.unNecessaryRows.get(i));
                for(int k=0;k < row.length;k++){
                    if(row[k]==1){
                        if(this.chromosomeCovering[k] == 1){
                            this.surplusIndividual++;
                        } else {
                            this.chromosomeCovering[k] = 1;
                        }
                    }
                }
                this.necessaryServices.add(unNecessaryRows.get(i));
            }
            if(allAreTrue(this.chromosomeCovering)){
                break;
            }
        }
    }  
    
    
    /**
     * sets covering from necessary rows to chromosomeCovering
     */
    public void setOriginCovering(){
        for(int i = 0;i<this.chromosomeCovering.length;i++){
            if(this.originCovering[i]==1){
                this.chromosomeCovering[i]=1;
            } else{
                this.chromosomeCovering[i]=0;
            }
        }
    }
    
    /**
     * 
     * @param list sort input list ascending
     */
    public void sortList(List<Integer> list){
        Collections.sort(list, new IntegerComparator());  
    }
    
    /**
     * generates values of chromosome
     */
    public void generateChromosome(){
        for(int i=0;i<this.chromosome.length;i++){
            this.chromosome[i] = getRandomBoolean();
        }
    }
    
    /**
     * 
     * @param list is list which i want to print to console for checking elements of list
     */
    public void printList(List<Integer> list){
        sortList(list);
        for(Integer element : list){
            System.out.print(element + " ");
        }
    }
    
    /**
     * 
     * @return true or false its random generator
     */
    public static boolean getRandomBoolean() {
       return rnd.nextBoolean();
    }

    public int[] getChromosomeCovering() {
        return chromosomeCovering;
    }
    
    public static Random getRnd(){
        return rnd;
    }
    
    /**
     * 
     * @return array of genes = chromosome
     */
    public boolean[] getChromosome(){
        return chromosome;
    }
    
    /**
     * 
     * @param array input array which i want to check if it contains only 1
     * @return true if array contains only 1 and false if it contains any value
     */    
    public boolean allAreTrue(int[] array){
        for(int i = 0;i<array.length;i++){
            if(array[i] == 0){
                return false;
            }
        }
        return true;
    }
    
    /**
     * 
     * @param index array index to set gene
     * @param booleanovka set specific gene to true or false
     */
    public void setGene(int index, boolean booleanovka){
        chromosome[index] = booleanovka;
    }
    
    /**
     * 
     * @param index which element of array i want to return
     * @return true or false (specific element of chromosome depends on index)
     */   
    public boolean getGene(int index){
        return chromosome[index];
    }

    public List<Integer> getNecessaryServices() {
        return necessaryServices;
    }
    
    /**
     * 
     * @return list of unnecessary rows from matrix it means rows which contains 1 in some columns where any other row didn't contain 1
     */
    public List<Integer> getUnNecessaryRows(){
        return unNecessaryRows;
    }
    
    /**
     * 
     * @return fitness value of specific chromosome (how good is this chromosome)
     */
    public int getFitnessValue(){
        return fitnessValue;
    }
    
    public void setMutationrate(double mutationRate){
        mutate.setMutation(mutationRate);
    }
    
    /**
     * evaluates individuals depends on how much services this solving needs for covering all customers
     */
    public void evaluateIndividual(){
        fitnessValue = 0;
        for(int i=0; i<necessaryServices.size(); i++){        
            this.fitnessValue += matrix.getCost()[necessaryServices.get(i)];
        }
    }

     /**
     * 
     * @param booleanek specific gene reverse his value
     * @return reversed boolean
     */
    public boolean reverseBoolean(boolean booleanek){
        return booleanek ^= true;
    }

    public Mutation getMutate() {
        return mutate;
    }

    public Matrix getMatrix() {
        return matrix;
    }
   
}
