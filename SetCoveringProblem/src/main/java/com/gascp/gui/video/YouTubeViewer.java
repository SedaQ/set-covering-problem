package com.gascp.gui.video;

//package com.mendelu.ga_scp.gui.video;
//
//import chrriis.dj.nativeswing.swtimpl.NativeInterface;
//import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
//import java.awt.BorderLayout;
//import javax.swing.JFrame;
//import javax.swing.JPanel;
//import javax.swing.SwingUtilities;
//import javax.swing.WindowConstants;
//
//
////required jars - DJNativeSwing-SWT.jar 
//                //DJNativeSwing.jar
//                //org.eclipse.swt.win32.win32.x86_64-4.2.jar   // tohle je platform dependent
//
//public class YouTubeViewer {
//
//    public YouTubeViewer(){
//    }
//    
//    public void startVideo() {
//        NativeInterface.open();
//        SwingUtilities.invokeLater(new Runnable() {
//            public void run() {
//                JFrame frame = new JFrame("YouTube Viewer");
//                frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
//                frame.getContentPane().add(getBrowserPanel(), BorderLayout.CENTER);
//                frame.setSize(800, 600);
//                frame.setLocationByPlatform(true);
//                frame.setVisible(true);
//            }
//        });
//        NativeInterface.runEventPump();
//        // don't forget to properly close native components
//        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
//            @Override
//            public void run() {
//                NativeInterface.close();
//            }
//        }));
//    }
//
//    public static JPanel getBrowserPanel(){
//        JPanel webBrowserPanel = new JPanel(new BorderLayout());
//        JWebBrowser webBrowser = new JWebBrowser();
//        webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
//        webBrowser.setBarsVisible(false);
//        
//        // za youtube.com/ musim vlozit:      v/b-   aby se mi to video zobrazilo pres celej jPanel
//        webBrowser.navigate("https://www.youtube.com/v/b-watch?v=AGTaPy7oH1s&feature=youtu.be");
//        return webBrowserPanel;
//    }
//  
//}
