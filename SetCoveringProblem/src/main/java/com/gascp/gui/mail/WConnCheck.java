package com.gascp.gui.mail;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author Seda
 */
public class WConnCheck {
    
    private final ConnectWebsite connectToWebsite;
    private final PageVerification verify;
    private String proxyHost;
    private String proxyPort;
    
    public WConnCheck(){
        connectToWebsite = new ConnectWebsite();
        verify = new PageVerification();
        proxyHost = "";
        proxyPort = "";
    }
    
    public boolean checkConnection(String webServer){
        verify.disableSslVerification();
        boolean actualStatus = false;
        int responseCode = 0;
        int attempts = 0;
        while (!actualStatus && attempts < 3) {
            try {
                responseCode = connectToWebsite.connectToPage(
                                webServer, this.proxyHost, this.proxyPort);
                // nez zkusim dalsi pokus, tak to uspim kdyby to byl jen chvilkovej vypadek
                if(responseCode!=200)
                    TimeUnit.SECONDS.sleep(10);
                System.out.println(responseCode);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (responseCode == 200){
                return true;
            }
            attempts++;
        }
        return false;
    }
    
    public static void main(String[] args){
        WConnCheck wc = new WConnCheck();
        boolean ahoj = wc.checkConnection("https://www.seznam.cz/");
        System.out.println("Seznam je dostupny nebo ne? " + ahoj);
    }
}
