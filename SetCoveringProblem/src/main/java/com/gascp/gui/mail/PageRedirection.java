package com.gascp.gui.mail;

import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

/**
 *
 * @author Seda
 */

public class PageRedirection {

	public PageRedirection() {
		
	}

	/**
	 * 
	 * @param responseCode check if responseCode from website is 301 (redirection) or not
	 * @return
	 */
	public boolean checkRedirection(int responseCode) {
		if (responseCode != HttpURLConnection.HTTP_OK) {
			if (responseCode == HttpURLConnection.HTTP_MOVED_TEMP
					|| responseCode == HttpURLConnection.HTTP_MOVED_PERM
					|| responseCode == HttpURLConnection.HTTP_SEE_OTHER) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param conn connection to redirected webpage
	 * @param proxyPort & proxyHost - connection with proxy
	 * @return responseCode from redirected page
	 * @throws Exception
	 */
	public int getRedirectResponseCode(HttpURLConnection conn,
			String proxyHost, String proxyPort) throws Exception {

		String redirectUrl = conn.getHeaderField("Location");
		System.out.println("Stranka: " + redirectUrl);

		// 
		HttpURLConnection connection = null;
		if ((!proxyHost.trim().equals("")) & (proxyPort.trim().equals(""))) {
			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(
					proxyHost, Integer.parseInt(proxyPort)));
			connection = (HttpURLConnection) new URL(redirectUrl)
					.openConnection(proxy);
		} else {
			connection = (HttpURLConnection) new URL(redirectUrl)
					.openConnection();
		}
                
		connection.setRequestMethod("HEAD");
		connection.setConnectTimeout(9000);
		connection.connect();
		int responseCode = connection.getResponseCode();

		return responseCode;
	}
}
