package com.gascp.gui.mail;

import com.gascp.gui.features.Regexs;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

public class SendEmail {

    private WConnCheck wc;
    private static String EOLN = System.getProperty("line.separator");
    private Regexs regex;

    public SendEmail() {
        regex = new Regexs();
    }

    /**
     * method which calls email validator in class Regexs
     *
     * @param email String representation of email for example
     * pavelseda@email.cz
     * @return if e-mail could exist or could
     */
    public boolean isEmailAddressValid(String email) {
        return regex.isEmailValid(email);
    }

    public boolean checkServerAvailability() {
        this.wc = new WConnCheck();
        boolean available = this.wc.checkConnection("https://www.seznam.cz/");
        return available;
    }

    /*
     public void sendEmail(String subject, String text, String contactAddress){
     String[] to = {"pavelseda@email.cz"};
     String host = "smtp.gmail.com";
     String from = "pavlikseda@gmail.com";
     String pass = "lukaspavel";
        
     Properties props = System.getProperties();
     props.put("mail.smtp.starttls.enable", "true");
     props.put("mail.smtp.host", host);
     props.put("mail.smtp.user", from);
     props.put("mail.smtp.password", pass);
     props.put("mail.smtp.ssl.trust", host);
     props.put("mail.smtp.port", "587");
     props.put("mail.smtp.auth", "true");

     Session session = Session.getDefaultInstance(props);
     MimeMessage message;

     try {
     message = new MimeMessage(session);
     message.setFrom(new InternetAddress(from));
     InternetAddress[] toAddress = new InternetAddress[to.length];

     // To get the array of addresses
     for( int i = 0; i < to.length; i++ ) {
     toAddress[i] = new InternetAddress(to[i]);
     }

     for( int i = 0; i < toAddress.length; i++) {
     message.addRecipient(Message.RecipientType.TO, toAddress[i]);
     }

     message.setSubject(subject);
     message.setText(text + EOLN + EOLN + contactAddress);
     Transport transport = session.getTransport("smtp");
     transport.connect(host, from, pass);
     transport.sendMessage(message, message.getAllRecipients());
     transport.close();
     }
     catch (AddressException ae) {
     ae.printStackTrace();
     }
     catch (MessagingException me) {
     me.printStackTrace();
     }
     }
     */
    /**
     * method which sends e-mail this method sends e-mail from my created e-mail
     * to my original mail
     *
     * @param subject of e-mail
     * @param text which you would like to send
     * @param contactAddress address of sender where i would like to response
     * him
     */
    public void sendEmail(String subject, String text, String contactAddress) {
        String to = "pavelseda@email.cz";
        final String from = "MailForApp@seznam.cz";
        final String pass = "tulakovaNoha";
        
        String host = "smtp.seznam.cz";
        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

        try {
            Properties props = new Properties();
            props.put("mail.smtp.host", host); // pro jinej server musim nastavit jinyho hosta treba pro yahoo bych musel dat smtp.mail.yahoo.com
            props.put("mail.smtp.auth", "true");
            //props.put("mail.debug", "true");
            props.put("mail.smtp.port", "465");
            //props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.ssl.trust", "smtp.seznam.cz");
            props.put("mail.smtp.socketFactory.class",
                    SSL_FACTORY);
            props.put("mail.smtp.socketFactory.port", "465");

            Session emailSession = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(
                                    from, pass);
                        }
                    });

            Message emailMessage = new MimeMessage(emailSession);
            emailMessage.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));

            // tenhle mail jsem vytvoril pro tuhle app, abych si z nej mohl posilat maily na muj originalni e-mail
            emailMessage.setFrom(new InternetAddress(from));
            emailMessage.setSubject(subject);
            emailMessage.setText(text + EOLN + EOLN + contactAddress);

            emailSession.setDebug(true);

            Transport.send(emailMessage);

        } catch (AddressException e) {
            printErrorMessage();
            e.printStackTrace();
        } catch (MessagingException e) {
            printErrorMessage();
            e.printStackTrace();
        }
    }

    public void printErrorMessage() {
        int pic = JOptionPane.ERROR_MESSAGE;
        JOptionPane.showMessageDialog(null, "Email was not sended", "", pic);
    }

    public void printPositiveMessage() {
        int pic = JOptionPane.INFORMATION_MESSAGE;
        JOptionPane.showMessageDialog(null, "Email was sended", "", pic);
    }
}
