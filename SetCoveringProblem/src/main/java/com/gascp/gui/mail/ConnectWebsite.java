package com.gascp.gui.mail;

import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

/**
 *
 * @author Seda
 */
public class ConnectWebsite {

    PageRedirection redirect;

    public ConnectWebsite() {
            redirect = new PageRedirection();
    }

    /**
     * 
     * @param url	
     * @param proxyHost
     * @param proxyPort
     * @return
     * @throws Exception
     */
    public int connectToPage(String url, String proxyHost, String proxyPort)
                    throws Exception {
            HttpURLConnection connection = null;

            // connection with proxy if proxyHost and proxyPort are empty values in XML then connect without proxy
            if ((!proxyHost.trim().equals("")) && (!proxyPort.trim().equals(""))) {
                    Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(
                                    proxyHost, Integer.parseInt(proxyPort)));
                    connection = (HttpURLConnection) new URL(url).openConnection(proxy);
            } else {
                    connection = (HttpURLConnection) new URL(url).openConnection();
            }

            // for connection without proxy
            // connection = (HttpURLConnection) new URL(url).openConnection();

            connection.setRequestMethod("GET");
            connection.setConnectTimeout(9000);
            connection.connect();

            int responseCode = connection.getResponseCode();

            // if responseCode = 301 (redirection) then check responseCode from redirected page
            if (redirect.checkRedirection(responseCode)) {
                    try {
                            responseCode = redirect.getRedirectResponseCode(connection,
                                            proxyHost, proxyPort);
                    } catch (Exception ex) {
                            System.out
                                            .println("Redirected URL is unable to connect: " + ex);
                            ex.printStackTrace();
                    }
            }
            return responseCode;
    }
}