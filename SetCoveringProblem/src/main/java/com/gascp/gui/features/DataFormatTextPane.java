package com.gascp.gui.features;

import com.gascp.matrix.ReadData;
import javax.swing.ImageIcon;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 *
 * @author Seda
 */

public class DataFormatTextPane {
    private javax.swing.JTextPane pane;

    public DataFormatTextPane(javax.swing.JTextPane pane){
        this.pane = pane;
        this.pane.setEditable(false);
    }
    
    /**
     * formates data in jTextPane in DataFormat class
     */
    public void format(){
        StyledDocument doc = pane.getStyledDocument();

        
        SimpleAttributeSet style = new SimpleAttributeSet();
        SimpleAttributeSet boldStyle = new SimpleAttributeSet();

        StyleConstants.setBold(style, true);
        StyleConstants.setFontSize(style, 14);
        
        StyleConstants.setBold(boldStyle, true);

        Style style2 = doc.addStyle("StyleName", null);
        StyleConstants.setIcon(style2, new ImageIcon("./src/main/resources/images/Annotation.jpg"));
        try {
            doc.insertString(0, "Annotation: " + ReadData.EOLN ,style2);
            doc.insertString(doc.getLength(), ReadData.EOLN, null);
            doc.insertString(doc.getLength(), "Columns ", boldStyle );
            doc.insertString(doc.getLength(), "- represent customers" + ReadData.EOLN, null );
            doc.insertString(doc.getLength(), "Rows ", boldStyle );
            doc.insertString(doc.getLength(), "- represents services" + ReadData.EOLN + ReadData.EOLN, null );
            doc.insertString(doc.getLength(), "Data should by only positive integer", null );
            doc.insertString(doc.getLength(), " separated with delimiter ;" + ReadData.EOLN + ReadData.EOLN, null );
            doc.insertString(doc.getLength(), "Data should have same number of columns in every line" + ReadData.EOLN, null );
            
        }
        catch(Exception e) { System.out.println(e); }
    }
}
