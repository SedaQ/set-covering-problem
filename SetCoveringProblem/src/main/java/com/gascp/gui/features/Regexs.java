package com.gascp.gui.features;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Seda
 */

public class Regexs {
    
    public Regexs(){}
    
    /**
     * 
     * @param email just String representation of email for example pavelseda@email.cz
     * @return returns if this e-mail is valid or invalid that means could exist or couldn't
     */
    public boolean isEmailValid(String email){     
        // testovaci string: pavlikseda@seznam.cz
        String emailRegex = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
        return Pattern.compile(emailRegex).matcher(email).matches();
    }
    
    /**
     * 
     * @param fileName is path to file in disk
     * @return not all path to file but only file name works only on windows operating systems
     */
    public String onlyFileName(String fileName){
        // chtelo by ho to vylepsit, takhle to matchuje jen windosackej typ cesty k souboru s timhle lomitkem \
        // (NOTE - az bude cas mrknu se nato)
        // test string: C:\SkolaMendelka\GA-SCP\src\dataFiles\testData3.csv
        String fileNameRegex = "^[\\w].+[\\w]\\\\(.*)";
        Matcher m = Pattern.compile(fileNameRegex).matcher(fileName);
        if(m.find()){
            System.out.println(m.group(1));
            return m.group(1);
        } 
        return fileName;
    }
    
    public String extractDigits(String item){
        String fileNameRegex = "(\\d+)";
        Matcher m = Pattern.compile(fileNameRegex).matcher(item);
        if(m.find()){
            System.out.println(m.group(1));
            return m.group(1);
        } 
        return item;
    }
}
