package com.gascp.gui.features;

import java.awt.event.KeyEvent;

public class OnlyDigitValidator {
    Beep b;
    
    /**
     * Constructor initialize Beep class
     */
    public OnlyDigitValidator(){
        b = new Beep();
    }
    
    /**
     * allows only digits to jTextField
     * @param evt event from GUI
     */
    public void onlyDigits(java.awt.event.KeyEvent evt){
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c)) || (c==KeyEvent.VK_BACK_SPACE) || c==KeyEvent.VK_DELETE){
            
            b.beep();
            evt.consume();
        }
    }
    
}
