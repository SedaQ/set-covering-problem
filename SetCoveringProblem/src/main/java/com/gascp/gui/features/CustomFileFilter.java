package com.gascp.gui.features;

import java.io.File;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

public class CustomFileFilter extends FileFilter{
    private List<String> extensions;
    private String description;
    
    public CustomFileFilter() {
        
    }

    /**
     * 
     * @param exts which extensions (přípony) could be seen
     * @param desc 
     */
    public CustomFileFilter(String[] exts, String desc) {
        if (exts != null) {
            extensions = new java.util.ArrayList<>();

            for (String ext : exts) {

                // Clean array of extensions to remove "."
                extensions.add(
                    ext.replace(".", "").trim().toLowerCase()
                );
            }
        } 
        description = (desc != null) ? desc.trim() : "Custom File List";
    }

    /**
     * Handles which files are allowed by filter.
     * @param f fileName
     * @return if file meet the conditions
     */
    @Override
    public boolean accept(File f) {
    
        // Allow directories to be seen.
        if (f.isDirectory()) return true;

        // exit if no extensions exist.
        if (extensions == null) return false;
		
        // Allows files with extensions specified to be seen.
        for (String ext : extensions) {
            if (f.getName().toLowerCase().endsWith("." + ext))
                return true;
        }

        // Otherwise file is not shown.
        return false;
    }

    /**
     * 
     * @return description
     */
    @Override
    public String getDescription() {
        return description;
    }
    
    /**
     * methot which returns filePath and uses methods above to filter files
     * @return path to file
     */
    public String getFilePathTextFiles(){
        JFileChooser chooser = new JFileChooser();
        File dataDirectory = new File("./src/main/java/transformedDataFiles");
        if(dataDirectory.exists() && dataDirectory.isDirectory()){
            chooser.setCurrentDirectory(dataDirectory);
        }
        chooser
            .addChoosableFileFilter(new CustomFileFilter(
                    new String[] { ".txt", ".DOC", ".DOCX",".csv" },
                    "Text Documents (*.DOC|DOCX|TXT|CSV)"));
        chooser.setAcceptAllFileFilterUsed(true);
        chooser.showOpenDialog(null);
        File f = chooser.getSelectedFile();
        String filePath = f.getAbsolutePath();
        return filePath;
    }
}
