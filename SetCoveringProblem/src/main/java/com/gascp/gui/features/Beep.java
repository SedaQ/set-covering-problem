package com.gascp.gui.features;

import java.applet.Applet;
import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Beep extends Applet implements ActionListener {
  Button b;

  /**
   * 
   * @param ae Overrides actionPerformed to do action (sound) i want
   */
  @Override
  public void actionPerformed (ActionEvent ae) {
    this.beep ();
  }

  /**
   * make sound when user press any non digit character to dMax and Generation field
   */
  public void beep() {
    this.getToolkit().beep();
  }

}
