package com.gascp.gui.graphs;

import java.util.List;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.ui.ApplicationFrame;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Seda
 */

public class LineChartGA extends ApplicationFrame {
    
   public LineChartGA(String applicationTitle)
   {
      super(applicationTitle);
   }

   private DefaultCategoryDataset createDataset(List<Double> list, List<Integer> list1)
   {
      DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
      for(int i = 0;i<list.size();i++){
        dataset.addValue(list.get(i) , "Average Fitness" , String.valueOf(i));
      }
      
      for(int i=0;i<list1.size();i++){
        dataset.addValue(list1.get(i) , "Best Fitness" , String.valueOf(i));  
      }
      return dataset;
   }
   
   public ChartPanel createChart(List<Double> list, List<Integer> list1,String chartTitle){
       JFreeChart lineChart = ChartFactory.createLineChart(
               chartTitle,
         "Generations","Fitness Value",
         createDataset(list,list1),
         PlotOrientation.VERTICAL,
         true,true,false);
       ChartPanel barPanel = new ChartPanel(lineChart);
       return barPanel;
   }

}
