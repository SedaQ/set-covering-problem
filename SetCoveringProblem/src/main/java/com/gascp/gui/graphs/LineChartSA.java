package com.gascp.gui.graphs;

import java.util.List;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;

/**
 *
 * @author Seda
 */

public class LineChartSA extends ApplicationFrame{
  
   public LineChartSA(String applicationTitle){
      super(applicationTitle);
   }

   private DefaultCategoryDataset createDataset(List<Integer> list){
      DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
      for(int i = 0;i<list.size();i++){
        dataset.addValue(list.get(i) , "Current State" , String.valueOf(i));
      }
      return dataset;
   }
   
   public ChartPanel createChart(List<Integer> list,String chartTitle){
       JFreeChart lineChart = ChartFactory.createLineChart(
               chartTitle,
         "Simulated Annealing","Current State Value",
         createDataset(list),
         PlotOrientation.VERTICAL,
         true,true,false);
       ChartPanel barPanel = new ChartPanel(lineChart);
       return barPanel;
   }
}
