package com.gascp.parsers.sax;

import com.gascp.parsers.Algorithm;
import com.gascp.parsers.DataProcessingInMemory;
import com.gascp.parsers.ErrorsHandler;
import com.gascp.parsers.IParser;
import com.gascp.parsers.sax.EverythingInMemorySAX;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 *
 * @author Seda
 */
public class SAXParser implements IParser {

    private File file;
    private EverythingInMemorySAX th;
    private ErrorsHandler eh;

    public SAXParser() {
        file = new File("./src/main/java/com/mendelu/ga_scp/dataStorageXML/DataStorage.xml");
        th = new EverythingInMemorySAX();
        eh = new ErrorsHandler();
    }

    @Override
    public void parse(File file) {
        try {
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setValidating(false);
            javax.xml.parsers.SAXParser saxLevel1 = spf.newSAXParser();
            XMLReader parser = saxLevel1.getXMLReader();
            parser.setErrorHandler(eh);    
            
            EverythingInMemorySAX sx = new EverythingInMemorySAX();
            parser.setContentHandler((ContentHandler) sx);
            parser.parse(file.getAbsolutePath());
            
            // v everything in memory si ulozim data vsechny data jako list objekt, které si pomocí getSeznam vytahnu a ulozim pak
            DataProcessingInMemory.printEverything(getXML(sx));
        // tenhle multicatch se používá až od 7čkové javy, tak je asi nekompatibilní se staršíma verzema nato bych si měl dát pozor, aby v tom nebyla náhodou nějaká chyba kdyžtak 
        } catch (SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }
    }
    
    public ArrayList<Algorithm> getXML(EverythingInMemorySAX sx){
        ArrayList<Algorithm> ar = sx.getSeznam();
        return ar;
    }
    
    public static void main(String[] args){
        SAXParser parserSax = new SAXParser();
        parserSax.parse(parserSax.getFile());
    }

    public File getFile() {
        return file;
    }
}