package com.gascp.parsers.sax;

import com.gascp.parsers.Algorithm;
import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Seda
 */
public class EverythingInMemorySAX extends DefaultHandler{
    
    private static final int BUFFER_SIZE = 1000;
    private StringBuffer value = new StringBuffer(BUFFER_SIZE);
    private boolean insideElement;
    private int id;
    private int dMax;
    private int numberOfServices;
    private String typeOfAlgorithm, fileName, serviceNames;
    private static ArrayList<Algorithm> ar = new ArrayList<>();
    
    public EverythingInMemorySAX(){}
    
    public ArrayList<Algorithm> getSeznam(){
        return ar;
    }
    
    @Override
    public void startDocument(){
        ar.clear();
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts){
        if(qName.equals("algorithm")){
            id = Integer.parseInt(atts.getValue("id"));
        }
        else if(qName.equals("dMax")){
            value.setLength(0);
            insideElement = true;
        }
        else if(qName.equals("typeOfAlgorithm")){
            value.setLength(0);
            insideElement = true;
        }
        else if(qName.equals("fileName")){
            value.setLength(0);
            insideElement = true;
        }
        else if(qName.equals("numberOfServices")){
            value.setLength(0);
            insideElement = true;
        }
        else if(qName.equals("serviceNames")){
            value.setLength(0);
            insideElement = true;
        }
    }
    
    @Override
    public void endElement(String uri, String localName, String qName){
        if(qName.equals("serviceNames")){
            serviceNames = value.toString();
            value.setLength(0);
            insideElement = true;
        }    
        else if(qName.equals("numberOfServices")){
            numberOfServices = Integer.parseInt(value.toString());
            value.setLength(0);
            insideElement = true;
        }
        else if(qName.equals("fileName")){
            fileName = value.toString();
            value.setLength(0);
            insideElement = true;
        }
        else if(qName.equals("typeOfAlgorithm")){
            typeOfAlgorithm = value.toString();
            value.setLength(0);
            insideElement = true;
        }
        else if(qName.equals("dMax")){
            dMax = Integer.parseInt(value.toString());
            value.setLength(0);
            insideElement = true;
        }
        else if(qName.equals("algorithm")){
            ar.add(new Algorithm(id, dMax, typeOfAlgorithm, fileName, numberOfServices, serviceNames));
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
            if (insideElement) {
                    value.append(ch, start, length);
            }
    }
}
