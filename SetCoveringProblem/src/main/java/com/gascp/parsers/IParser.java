package com.gascp.parsers;

import java.io.File;

/**
 *
 * @author Seda
 */
public interface IParser {

    public void parse(File file);

}
