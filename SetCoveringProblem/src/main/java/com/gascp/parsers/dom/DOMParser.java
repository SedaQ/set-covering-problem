/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gascp.parsers.dom;

import com.gascp.parsers.Algorithm;
import com.gascp.parsers.DataProcessingInMemory;
import com.gascp.parsers.ErrorsHandler;
import com.gascp.parsers.IParser;
import com.gascp.parsers.dom.EverythingInMemoryDOM;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author Seda
 */
public class DOMParser implements IParser {
    
    private File file;
    private ErrorsHandler eh;
    
    public DOMParser(){
        file = new File("./src/main/java/com/mendelu/ga_scp/dataStorageXML/DataStorage.xml");
        eh = new ErrorsHandler();
    }
    
    public static void main(String[] args){
        DOMParser dp = new DOMParser();
        dp.parse(dp.getFile());
    }

    @Override
    public void parse(File file) {
        try {
            
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setValidating(false);
            dbf.setIgnoringElementContentWhitespace(true);
            DocumentBuilder builder = dbf.newDocumentBuilder();
            builder.setErrorHandler(eh);
            Document doc = builder.parse(file.getAbsolutePath());
            
            ArrayList<Algorithm> ar = EverythingInMemoryDOM.getSeznam(doc);
            DataProcessingInMemory.printEverything(ar);
            
            
            // v 7ckove jave se da pouzit vsechny Exceptny v 1 catch blocku a zretezit je pomoci |
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }
    }
    
    public File getFile(){
        return file;
    }
}
