package com.gascp.parsers.dom;

import com.gascp.parsers.Algorithm;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import org.xml.sax.SAXException;

/**
 *
 * @author Seda
 */

public class DomWriter {
    
    public DomWriter(){}

    public void write(File f,String dMax2,String typeOfAlgorithm2,String fileName2,String numberOfServices2,String[] serviceNames2) throws TransformerConfigurationException, TransformerException, ParserConfigurationException, SAXException, IOException{
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document doc = documentBuilder.parse(f.getAbsolutePath());
        
       // pro iteraci attributu id v elementu algorithm
        ArrayList<Algorithm> ar = EverythingInMemoryDOM.getSeznam(doc);
        int s = ar.size()-1;
        String newID = String.valueOf(ar.get(s).getId() + 1);
        
        // Root Element
        Element rootElement = doc.getDocumentElement();

        // odrakovavani;
        Text textCRLF = doc.createTextNode("    ");
        // algorithm elements
        Element algorithm = doc.createElement("algorithm");
        rootElement.appendChild(textCRLF);
        algorithm.setAttribute("id", newID);
        rootElement.appendChild(algorithm);

        Element dMax = doc.createElement("dMax");
        dMax.appendChild(doc.createTextNode(dMax2));
        algorithm.appendChild(dMax);

        Element typeOfAlgorithm = doc.createElement("typeOfAlgorithm");
        typeOfAlgorithm.appendChild(doc.createTextNode(typeOfAlgorithm2));
        algorithm.appendChild(typeOfAlgorithm);

        Element fileName = doc.createElement("fileName");
        fileName.appendChild(doc.createTextNode(fileName2));
        algorithm.appendChild(fileName);

        Element numberOfServices = doc.createElement("numberOfServices");
        numberOfServices.appendChild(doc.createTextNode(numberOfServices2));
        algorithm.appendChild(numberOfServices);

        Element serviceNames = doc.createElement("serviceNames");
        serviceNames.appendChild(doc.createTextNode(convertStringArrayToString(serviceNames2)));
        algorithm.appendChild(serviceNames);

        rootElement.appendChild(algorithm);

        DOMSource source = new DOMSource(doc);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        
        StreamResult result = new StreamResult(f);
        transformer.transform(source, result);
    }
    
    public String convertStringArrayToString(String[] array){
        StringBuilder builder = new StringBuilder();
        for(String s : array) {
            builder.append(s);
        }
        return builder.toString();
    }       
    
}