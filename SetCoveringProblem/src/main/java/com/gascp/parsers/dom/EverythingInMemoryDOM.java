/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gascp.parsers.dom;

import com.gascp.parsers.Algorithm;
import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Seda
 */
public class EverythingInMemoryDOM {
    
    private static ArrayList<Algorithm> ar = new ArrayList<Algorithm>();
    
    public static ArrayList<Algorithm> getSeznam(Document doc){
        NodeList nl = doc.getElementsByTagName("algorithm");
        for(int i = 0; i<nl.getLength();i++){
            Node e = nl.item(i);
            if(e.getNodeType() == Node.ELEMENT_NODE){
                ar.add(createAlgorithm(e));
            }
        }
        return ar;
    } 
    
    private static Algorithm createAlgorithm(Node algorithm){
        String value = ((Element) algorithm).getAttributeNode("id").getValue();
        int id = Integer.parseInt(value);
        int dMax = getDMax(algorithm);
        String typeOfAlgorithm = getTypeOfAlgorithm(algorithm);
        String fileName = getFileName(algorithm);
        int numberOfServices = getNumberOfServices(algorithm);
        String serviceNames = getServiceNames(algorithm);
        
        return new Algorithm(id, dMax, typeOfAlgorithm, fileName, numberOfServices, serviceNames);
    }
    
    private static Node getNodeByName(Node parent, String name){
        NodeList nl = parent.getChildNodes();
        for(int i = 0; i<nl.getLength(); i++){
            Node e = nl.item(i);
            if(e.getNodeName().equals(name) == true){
                return e;
            }
        }
        return null;
    }
    
    private static int getDMax(Node algorithm){
        Node dMax = getNodeByName(algorithm,"dMax");
        Node text = getNodeByName(dMax, "#text");
        String value = text.getNodeValue().trim();
        return Integer.parseInt(value); 
    }
    
    private static String getTypeOfAlgorithm(Node algorithm){
        Node typeOfAlgorithm = getNodeByName(algorithm,"typeOfAlgorithm");
        Node text = getNodeByName(typeOfAlgorithm, "#text");
        String value = text.getNodeValue().trim();
        return value;
    }

    private static String getFileName(Node algorithm){
        Node fileName = getNodeByName(algorithm,"fileName");
        Node text = getNodeByName(fileName, "#text");
        String value = text.getNodeValue().trim();
        return value;
    }
    
    private static int getNumberOfServices(Node algorithm){
        Node numberOfServices = getNodeByName(algorithm,"numberOfServices");
        Node text = getNodeByName(numberOfServices, "#text");
        String value = text.getNodeValue().trim();
        return Integer.parseInt(value); 
    }
    
    private static String getServiceNames(Node algorithm){
        Node serviceNames = getNodeByName(algorithm,"serviceNames");
        Node text = getNodeByName(serviceNames, "#text");
        String value = text.getNodeValue().trim();
        return value;
    }
              
}
