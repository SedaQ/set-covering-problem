/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gascp.parsers;

import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;
import java.util.regex.Pattern;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author SedaQ
 */
public class FillServiceCentresCustomerLocations {

    private List<String> customerLoc = new ArrayList<>();
    private List<String> serviceCentresList = new ArrayList<>();
    private Map<String, List<Long>> matrix = new HashMap<>();
    
    public static void main(String[] args){
        //Path path = Paths.get("./src/main/resources/blansko/service-centres-and-customer-locations-googlesheets.csv");
        Path path = Paths.get("./src/main/resources/blansko/data-blansko-all-schools-and-villages-csv.csv");
        
        FillServiceCentresCustomerLocations fsccl = new FillServiceCentresCustomerLocations();
        fsccl.parseServiceCentresAndCustomerLocations(path);
        
        fsccl.fillDistanceMatrix(fsccl.customerLoc, fsccl.serviceCentresList);
        
        fsccl.matrix.forEach((k,v) -> System.out.println(k + ": values: " + v));
    }
    
    private void fillDistanceMatrix(List<String> customerLoc, List<String> serviceCentresList){
        // https://batchgeo.com/ - for ploting addresses to google map
        // https://chromedriver.storage.googleapis.com/index.html?path=2.42/   - driver for Selenium
        
        // http://www.vzdalenostmest.cz/   - page to invoke particular actions
        System.setProperty("webdriver.chrome.driver","./src/main/resources/chrome-driver/chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        
        serviceCentresList.forEach(scl -> {
            List<Long> kilometers = new ArrayList<>();
            StringBuilder sb = new StringBuilder();
            customerLoc.forEach(cl ->{
                driver.navigate().to("http://www.vzdalenostmest.cz/");
                WebElement distanceFrom = driver.findElement(By.id("from"));
                distanceFrom.sendKeys(scl);
                WebElement distanceTo = driver.findElement(By.id("to"));
                distanceTo.sendKeys(cl);

                driver.findElement(By.xpath("//input[@class='submit' and @type='submit']")).click();

                WebElement results = driver.findElement(By.xpath("//span[@class='routeDistance']"));

                WebDriverWait wait = new WebDriverWait(driver, 180);
                wait.until(ExpectedConditions.textMatches(By.xpath("//span[@class='routeDistance']"), Pattern.compile("\\w+")));
                Long km = Math.round(Double.parseDouble(results.getText()));
                kilometers.add(km);
                sb.append(km).append(",");
            });
            System.out.println(sb.toString());
            matrix.put(scl, kilometers);
        });
        
        //System.out.println(submitResults);
                
    }
    
    private void parseServiceCentresAndCustomerLocations(Path path){
                try(BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8)){
            String line = "";
            //String[] customerLocations = br.readLine().split(Pattern.quote(","));
            String[] customerLocations = br.readLine().split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
            String[] newArrayWithoutFirst = Arrays.copyOfRange(customerLocations, 1, customerLocations.length);
            for(int i=0;i<newArrayWithoutFirst.length;i++){
                newArrayWithoutFirst[i] = newArrayWithoutFirst[i].substring(1, newArrayWithoutFirst[i].length()-1);
                //newArrayWithoutFirst[i] = newArrayWithoutFirst[i].replace("?", "ř");
            }
            
            customerLoc = Arrays.asList(newArrayWithoutFirst);
            customerLoc.forEach(System.out::println);
            
            System.out.println(System.lineSeparator());
            System.out.println(System.lineSeparator());

            while((line=br.readLine()) != null){
                String[] serviceCentres = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
                serviceCentresList.add(serviceCentres[0].substring(1, serviceCentres[0].length()-1));
            }
            serviceCentresList.forEach(System.out::println);
        } catch(IOException io){
            io.printStackTrace();
        }

    }
}
