package com.gascp.parsers;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * @author Seda
 */

public class ErrorsHandler implements ErrorHandler {

    private String message(SAXParseException e) {
            return e.getSystemId() + "\n" + "radka: " + e.getLineNumber()
                            + " sloupec: " + e.getColumnNumber() + "\n" + e.getMessage();
    }

    @Override
    public void warning(SAXParseException e) {
            System.out.println("Warning: " + message(e));
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        throw new SAXException("Error: " + message(e));
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
            throw new SAXException("Kritical error: " + message(e));
    }

}