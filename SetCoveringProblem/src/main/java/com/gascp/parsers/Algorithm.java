package com.gascp.parsers;

import com.gascp.matrix.ReadData;

/**
 *
 * @author Seda
 */

public class Algorithm {
    
    private int id;
    private int dMax;
    private String typeOfAlgorithm;
    private String fileName;
    private int numberOfServices;
    private String serviceNames;
    
   public Algorithm(int id, int dMax, String typeOfAlgorithm, String fileName, int numberOfServices, String serviceNames){
        this.id=id;
        this.dMax=dMax;
        this.typeOfAlgorithm = typeOfAlgorithm;
        this.fileName = fileName;
        this.numberOfServices = numberOfServices;
        this.serviceNames = serviceNames;
    } 
    
    @Override
    public String toString(){
        return "Algoritmus cislo: " + id + ReadData.EOLN + "Zvolene dMax bylo: " + dMax + ReadData.EOLN + "Typ zvoleneho algoritmu: " + typeOfAlgorithm + ReadData.EOLN +
                "Jmeno pouziteho souboru: " + fileName + ReadData.EOLN + "Pocet potrebnych sluzeb: " + numberOfServices + ReadData.EOLN + "Potrebne sluzeb: " + serviceNames + ReadData.EOLN;
    }

    public int getId() {
        return id;
    }
    
}
