package com.gascp.parsers;

import com.gascp.matrix.ReadData;
import java.util.ArrayList;

/**
 *
 * @author Seda
 */
public class DataProcessingInMemory {
    
    public static StringBuilder printEverything(ArrayList<Algorithm> ar){
        StringBuilder sb = new StringBuilder();
        for(Algorithm algo: ar){
            sb.append(algo + ReadData.EOLN);
            //System.out.println(algo);
        }
        return sb;
    }    
}
