package com.gascp.parsers.xlstTransform;

import java.io.File;
import java.io.FileOutputStream;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

/**
 *
 * @author Seda
 */

public class TransformXLST {
    public TransformXLST(){}
  
    public void transform(File f1, File f2, File f3) {
        try {
            TransformerFactory tFactory = TransformerFactory.newInstance();

            Transformer transformer = tFactory.newTransformer
               (new javax.xml.transform.stream.StreamSource
                  (f1.getAbsolutePath()));

          transformer.transform
            (new javax.xml.transform.stream.StreamSource
                  (f2.getAbsolutePath()),
             new javax.xml.transform.stream.StreamResult
                  ( new FileOutputStream(f3.getAbsolutePath())));
          }
        catch (Exception e) {
          e.printStackTrace( );
        }
    }
}

