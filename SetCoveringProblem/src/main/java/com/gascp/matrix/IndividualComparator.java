package com.gascp.matrix;

import com.gascp.population.Individual;
import java.util.Comparator;

/**
 *
 * @author Seda
 */
public class IndividualComparator  implements Comparator<Individual> {

    @Override
    public int compare(Individual t1, Individual t2) {
        Integer fitnessValue1 = t1.getFitnessValue();
        Integer fitnessValue2 = t2.getFitnessValue();
        
        //descending order
        //return fitnessValue2.compareTo(fitnessValue1);
        
        //ascending order
        return fitnessValue1.compareTo(fitnessValue2);
    }
    
}
