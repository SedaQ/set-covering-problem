package com.gascp.matrix;

import java.util.List;

/**
 *
 * @author Seda
 */
public interface IAlgorithm {
    
    public void startAlgorithm();
    public List<Double> getAverageFitness();   
    public List<Integer> getBestFitnessList();
    
}
