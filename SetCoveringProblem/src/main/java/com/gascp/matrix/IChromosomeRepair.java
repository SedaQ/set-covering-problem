package com.gascp.matrix;

/**
 *
 * @author Seda
 */

public interface IChromosomeRepair {
    
    public boolean allAreTrue(int[] array);
    public void repairOperator();
    
}
