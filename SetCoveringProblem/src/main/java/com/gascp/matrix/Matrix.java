package com.gascp.matrix;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;

/**
 *
 * @author Seda
 */

public class Matrix {
    
    private int[][] originMatrix;
    private int[][] matrix;
    private ReadData rd;
    private String DELIMITER;
    private int rows;
    private int columns;
    private int dMax;
    private int[] NRowsContainsColumns;
    private List<Integer> unNecessaryRows;
    private ArrayList<Integer> necessaryRows;
    private List<Integer> removedRows;
    private Map<Integer, List<Integer>> columnsContainsNcRows;
    private int[] cost;
    private int surplus = 0;
    private boolean validDmax;
    
    public Matrix(int dMax){
        rd = new ReadData();
        this.dMax = dMax;
        DELIMITER = ";";
        columnsContainsNcRows = new HashMap<>();
        unNecessaryRows = new ArrayList<>(); 
        necessaryRows= new ArrayList<>();
        removedRows  = new ArrayList<>();
    }

    /**
     * method transform matrix to reachAbilityMatrix (contains only 0 and 1 depends on dMax value)
     * @param file 
     * @param delimiter
     * @return 
     */
    public int[][] reachAbilityMatrix(File file, String delimiter){
        this.originMatrix = rd.getDataIntoMatrix(file, delimiter);
        int[][] transformedMatrix  = new int[originMatrix.length][originMatrix[0].length];
        int suma = 0;
        
        for(int i=0; i<originMatrix.length;i++){
            for(int j=0; j<originMatrix[0].length;j++){
                if(originMatrix[i][j] > dMax){
                    transformedMatrix[i][j] = 0;
                } else {
                    transformedMatrix[i][j] = 1;
                    suma +=1;
                }       
            }
            if(suma == 0){
                // deletnout tento radek z matice
                //System.arraycopy()
                removedRows.add(i);
            }
            suma=0;
        }
       
        /*
        if(removedRows.size()>0){
            matrix = removeSpecificRowFromMatrix(transformedMatrix, removedRows);
            //System.out.println("Tady se mel zrusit radek");
        } else{
            matrix = transformedMatrix;
        }
        */
        
        matrix = transformedMatrix;
        this.rows = matrix.length;
        this.columns = matrix[0].length;
        NRowsContainsColumns = new int[columns];
        
        System.out.println("Matice vzdalenosti: " + Arrays.deepToString(originMatrix));
        System.out.println("Matice dostupnosti: " + Arrays.deepToString(matrix));
        
        return matrix;
    }

    /**
     * checks if input data are valid for setted dMax value 
     * @param file
     * @param delimiter
     * @return true if matrix is without column which contain only zero values
     */
    public boolean isDMaxValid(File file, String delimiter){
        int[][] transformedMatrix = reachAbilityMatrix(file, delimiter);
        //System.out.println(Arrays.deepToString(transformedMatrix));
        int suma = 0;
        int m = 0;
        for(int i=0;i<transformedMatrix[0].length;i++){
            for(int j=0;j<transformedMatrix.length;j++){
                if(transformedMatrix[j][i] == 1){
                    suma = suma + 1;
                    m=j;
                } 
            }
            if(suma == 1) {
                saveToNecessaryRows(m);

            } else if(suma == 0) {
                System.out.println("DMax pro danou matici je nevyhovujici: ");
                int pic;
                pic = JOptionPane.ERROR_MESSAGE;
                JOptionPane.showMessageDialog(null,"Change DMax value with this DMax some customer isnt served","",pic);
                return false;
            }
            suma=0;
        }
        
        // pokud radek nebyl oznacen za nezbytny, tak je ulozen do radku, ktere tvori chromosome
        saveToUnNecessaryRows();
        // odstrani radky, ktere byly odstraneny neboli budou ignorovany z nepotrebnych sluzeb
        removeRemovedRowsFromUnNecessaryRows();
        
        System.out.println("Matice dostupnosti ma radku: " + matrix.length);
        System.out.print("Nezbytne radky jsou z matice dostupnosti: ");
        sortList(this.necessaryRows);
        printList(this.necessaryRows);
        System.out.println();
        
        System.out.print("Radky, ktere nejsou potreba z matice vzdalenosti (byly odstraneny a nebudou se ucastnit krizeni): ");
        printList(this.removedRows);
        System.out.println();
        
        System.out.print("Radky, ktere tvori chromosome, cili ty, ktere nejsou nezbytne z matice dostupnosti: ");
        printList(this.unNecessaryRows);
        System.out.println();
        
        // nastavi hodnotu jednotlivych obsluznych mist 
        this.cost = new int[this.matrix.length];
        setCost();
        
        System.out.println("Vahy jednotlivych sluzeb: " + Arrays.toString(cost));
        
        //saveColumnsOfNecessaryRows(necessaryRows);       
        saveColumnsOfNecessaryRows(this.necessaryRows);
        removeRemovedRowsFromUnNecessaryRows();
        printMap(this.columnsContainsNcRows);
        unNRowsContainsColumns(this.columnsContainsNcRows);
        System.out.println("Zakaznici, ktere pokryvaji nezbytne sluzby jsou: " + Arrays.toString(NRowsContainsColumns) + ReadData.EOLN);
        //System.out.println("Mapa radku, ktere jsou nezbytne a sloupce, ktere obsahuji: " + Arrays.toString(columnsContainsNcRows));
        
        this.validDmax = true;
        return true;
    }
        
    public void removeRemovedRowsFromUnNecessaryRows(){
        for(int i = 0; i<this.removedRows.size();i++){
            this.unNecessaryRows.remove(this.removedRows.get(i));
        }
    }
    
   public void setCost(){
        for(int i =0;i<this.cost.length;i++){
            this.cost[i] = 1;
        }
    } 
    
    private void saveToNecessaryRows(int m){
        this.necessaryRows.add(m);
    }
    
    public void sortList(List<Integer> list){
        Collections.sort(list, new IntegerComparator());
        for (Integer integer : list) {
             //System.out.println(integer);
        }        
    }
    
    private void saveToUnNecessaryRows(){
        for(int n=0;n<rows;n++){
            if(!this.necessaryRows.contains(n)){
                this.unNecessaryRows.add(n);
            }
        }
    }

    public void printMap(Map<Integer,List<Integer>> map){
        for(Map.Entry<Integer,List<Integer>> entry : map.entrySet()){
            Integer key = entry.getKey();
            List<Integer> value = entry.getValue();
            for(int i =0; i < value.size(); i++){
            }
            System.out.println("Nezbytny radek je: " + key + " a sloupce, ktere obsahuje: " + value.toString());
        }
    }
    
    // vytvori pole, kterej obsahuje pokryti zakazniku nezbytnymi sluzbami
    public void unNRowsContainsColumns(Map<Integer,List<Integer>> map){
        for(Map.Entry<Integer,List<Integer>> entry : map.entrySet()){
            Integer key = entry.getKey();
            List<Integer> value = entry.getValue();
            for(int i=0;i<value.size();i++){
                for(int n=0;n<this.columns;n++){
                    if(n==value.get(i)){
                        if(this.NRowsContainsColumns[n]==1){
                            this.surplus++;
                        } else{
                            this.NRowsContainsColumns[n] = 1;
                        }
                    }
                }
            }
        }
    }
    
    /**
     * method to remove specific row from matrix
     * @param array array in which some specific row will be removed
     * @param row specific row to remove
     * @return new array without specific row
     */
    public int[][] removeSpecificRowFromMatrix(int[][] array, List<Integer> REMOVE_ROWS){
        int[][] returnArray = new int[array.length-REMOVE_ROWS.size()][array[0].length];
        int m=0;
        for(int i = 0; i<array.length;i++){
            if(REMOVE_ROWS.contains(i)){
                continue;
            }
            int n=0;
            for(int j =0; j<array[0].length;j++){
                returnArray[m][n] = array[i][j];
                ++n;
            }           
            ++m;
        }
        
        return returnArray;
    }
    
    /**
     * 
     * @param list 
     */
    public void saveColumnsOfNecessaryRows(ArrayList<Integer> list){
        ArrayList<Integer> helper = new ArrayList<>();
        for(int i = 0; i<list.size();i++){
            System.out.println();
            //System.out.println("Delka listu je: " + list.size());
            int[] row = new int[this.matrix.length];
            row = getRowFromMatrix(this.matrix, list.get(i));
            //System.out.println("Hodnoty radku z matice " + Arrays.toString(row));
            for(int j=0; j<row.length;j++){
                if(row[j]==1){
                    helper.add(j);
                }
            }
            this.columnsContainsNcRows.put(list.get(i),(ArrayList) helper.clone());
            helper.clear();
            //columnsContainsNcRows.put(necessaryRows.get(i),(ArrayList) list.clone());
            //list.clear();
        }
    }
 
    /**
     * print to console content of specific input list
     * @param list input collection to be printed
     */
    public void printList(List<Integer> list){
        for(Integer element : list){
            System.out.print(element + " ");
        }
    }
    
    /**
     * method to return specific row depends on parametr radek from first input parametr matrix
     * @param matrix input matrix
     * @param radek input row which should be returned
     * @return 
     */
    public int[] getRowFromMatrix(int[][] matrix, Integer radek){
        // pocet radku
        int row = matrix.length;     
        // pocet kolonek
        int columns = matrix[0].length;
        int rows[] = new int[columns];
        for(int i=0;i<columns;i++){
            if(radek>row || radek<0){
                System.out.println("V metode getRowFromMatrix, ktera je ve tride Matrix, chci vratit radek matice, ktery je vetsi nez pocet radku matice nebo naopak mensi nez pocet radku matice");
            } else { 
                rows[i] = matrix[radek][i];
            }
        }
        
        return rows;
    }
    
    public int initializeFitness(){
        int fitnessValue = 0;
        for(int i=0; i<getUnNecessaryRows().size(); i++){        
            fitnessValue += getCost()[getUnNecessaryRows().get(i)];
        }   
        return fitnessValue;
    }
    
    public int getRows(){
        return rows;
    }
    
    public int getColumns(){
        return columns;
    }
    
    public int getMatrixRowsLength(){
        return matrix.length;
    }
   
    public int getMatrixColumnsLength(){
        return matrix[0].length;
    }
    
    public ArrayList<Integer> getNecessaryRows(){
        return necessaryRows;
    }
    
    public List<Integer> getUnNecessaryRows(){
        return unNecessaryRows;
    }
    
    public int getSurplus(){
        return surplus;
    }
    
    public int[][] getMatrix(){
        return matrix;
    }
    
    public int[] getUNContainsColumns(){
        return NRowsContainsColumns;
    }
    
    public List<Integer> getRemovedRows(){
        return removedRows;
    }
    
    public int[] getCost(){
        return cost;
    }

    public boolean isValidDmax() {
        return validDmax;
    }
}
