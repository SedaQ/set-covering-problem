/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gascp.matrix;

import java.util.Comparator;

/**
 *
 * @author Seda
 */
public class IntegerComparator implements Comparator<Integer> {
 
    /**
     * method sorting List collection ascending (NOTE - pro descending sort zmenim o2>o1 na o1>o2)
     * @param o1 first Integer
     * @param o2 second Integer
     * @return 
     */
    @Override
    public int compare(Integer o1, Integer o2) {
        //seradi od nejmensiho po nejvetsiho
        return (o2>o1 ? -1 : (o1==o2 ? 0 : 1));
    }

}
