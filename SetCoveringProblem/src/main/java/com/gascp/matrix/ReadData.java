package com.gascp.matrix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Seda
 */

public class ReadData {
    
    public static String EOLN = System.getProperty("line.separator");
    
    public ReadData(){
        
    }
    
    /**
     * 
     * @param file path to file which i want to parse
     * @param delimiter delimiter to split values into matrix
     * @return 
     */
    // mozna nebude na skodu udelat teto metode druhy parametr delimiter, abych tam dosadil 
    // jine druhy delimieteru, ktere si treba uzivatel vybere v GUicku
    public int[][] getDataIntoMatrix(File file, String delimiter){
        BufferedReader r = null;
        String line = null;
        List<String[]> list = new ArrayList<>();
        int[][] vysl = null;

        try {
            r = new BufferedReader(new FileReader(file.getAbsolutePath()));
            while((line = r.readLine()) != null){
                if(!line.startsWith("#")){
                    String[] cols = line.split(delimiter);
                    list.add(cols); 
                }
            }       
            int rows = list.size();
            int columns = list.get(0).length;
            vysl = new int[rows][columns];

            try {
                // put values from ArrayList<String[]> to two dimensional array String[][]
                for(int i = 0; i<rows;i++){
                    for(int j = 0; j<columns; j++){
                        try{
                        vysl[i][j] = Integer.parseInt(list.get(i)[j].trim());
                        } catch(NumberFormatException ex){
                            int pic;
                            pic = JOptionPane.ERROR_MESSAGE;
                            JOptionPane.showMessageDialog(null,"Input data dont contains only numbers with delimiters","",pic);
                            return null;
                        }
                    }
                }
            } catch(ArrayIndexOutOfBoundsException ex){
                int pic;
                pic = JOptionPane.ERROR_MESSAGE;
                JOptionPane.showMessageDialog(null,"Input data doesnt match CSV rules (same number of rows and columns","",pic);
                return null;
            } catch(Exception ex){
                System.out.println("Data maji spatny format" + ex);
                return null;
            }
            
            //System.out.println("Original data: " + Arrays.deepToString(vysl));
        } catch(IOException io ){
            io.printStackTrace();
        } catch(Exception ex){
            ex.printStackTrace();
        } finally {
            if(r !=null){
                try {
                    r.close();
                } catch (IOException ex) {
                    Logger.getLogger(ReadData.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return vysl;
    }
}
