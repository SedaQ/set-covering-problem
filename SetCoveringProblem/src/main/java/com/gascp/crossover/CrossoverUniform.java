package com.gascp.crossover;

import com.gascp.population.Individual;
import com.gascp.matrix.Matrix;

/**
 *
 * @author Seda
 */
public class CrossoverUniform implements ICrossover{

    private final double ratio = 0.5;
    
    @Override
    public Individual[] crossover(Matrix matrix, Individual ind1, Individual ind2) {
        Individual[] offsprings = new Individual[2];
        offsprings[0] = new Individual(matrix);
        offsprings[1] = new Individual(matrix);
        
        double rnd;
        int individualSize = ind1.getMatrix().getUnNecessaryRows().size();
        
        for(int i=0;i<individualSize;i++) {
            rnd = ind1.getRnd().nextDouble();
            if(rnd < this.ratio){
                offsprings[0].setGene(i, ind2.getGene(i));
                offsprings[1].setGene(i, ind1.getGene(i));    
            } else {
                offsprings[0].setGene(i, ind1.getGene(i));
                offsprings[1].setGene(i, ind2.getGene(i));      
            }
        }
        
        offsprings[0].setChromosomeCovering();
        offsprings[0].addNecessaryRowsToNServices();
        offsprings[0].setMutationrate(ind1.getMutate().getMutationRate());
        offsprings[1].setChromosomeCovering();
        offsprings[1].addNecessaryRowsToNServices();
        offsprings[1].setMutationrate(ind1.getMutate().getMutationRate());
        
        return offsprings;
    }
  
}
