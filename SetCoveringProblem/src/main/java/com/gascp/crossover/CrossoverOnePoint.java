package com.gascp.crossover;

import com.gascp.population.Individual;
import com.gascp.ga.selection.RouletteWheelSelection;
import com.gascp.matrix.Matrix;

/**
 *
 * @author Seda
 */

public class CrossoverOnePoint implements ICrossover{
    
    public CrossoverOnePoint(){}

    @Override
    public Individual[] crossover(Matrix matrix, Individual ind1,Individual ind2) {
        Individual[] offsprings = new Individual[2];
        offsprings[0] = new Individual(matrix);
        offsprings[1] = new Individual(matrix);

        int individualSize = ind1.getMatrix().getUnNecessaryRows().size()-1;
        
        int rnd = RouletteWheelSelection.randInt(0, individualSize);
        int i;
        
        for (i=0; i<rnd; ++i) {
            offsprings[0].setGene(i, ind1.getGene(i));
            offsprings[1].setGene(i, ind2.getGene(i));
        }  
        for (i=rnd; i<individualSize; ++i) {
            offsprings[0].setGene(i, ind2.getGene(i));
            offsprings[1].setGene(i, ind1.getGene(i));
        }

        offsprings[0].setChromosomeCovering();
        offsprings[0].addNecessaryRowsToNServices();
        offsprings[0].setMutationrate(ind1.getMutate().getMutationRate());
        offsprings[1].setChromosomeCovering();
        offsprings[1].addNecessaryRowsToNServices();
        offsprings[1].setMutationrate(ind1.getMutate().getMutationRate());
        
        return offsprings;
    }

}
