package com.gascp.crossover;

import com.gascp.population.Individual;
import com.gascp.matrix.Matrix;

/**
 *
 * @author Seda
 */
public interface ICrossover {
    
    public Individual[] crossover(Matrix matrix, Individual ind1, Individual ind2);
    
}
