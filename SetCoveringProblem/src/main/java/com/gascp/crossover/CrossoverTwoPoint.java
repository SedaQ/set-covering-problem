package com.gascp.crossover;

import com.gascp.population.Individual;
import com.gascp.ga.selection.RouletteWheelSelection;
import com.gascp.matrix.Matrix;

/**
 *
 * @author Seda
 */
public class CrossoverTwoPoint implements ICrossover {
    
    public CrossoverTwoPoint(){    
    }

    @Override
    public Individual[] crossover(Matrix matrix, Individual ind1,Individual ind2) {
        Individual[] offsprings = new Individual[2];
        offsprings[0] = new Individual(matrix);
        offsprings[1] = new Individual(matrix);

        int individualSize = ind1.getMatrix().getUnNecessaryRows().size()-1;
        
        int rnd1 = RouletteWheelSelection.randInt(0, individualSize);
        int rnd2 = RouletteWheelSelection.randInt(0, individualSize);
        
        // kdybych mel prvni split point vetsi nez ten druhej, tak je musim vymenit
        if(rnd1>rnd2){
           swap(rnd1,rnd2);
        } else if(rnd1 == rnd2){
            ++rnd2;
        }
        
        int i;
        for (i=0; i<rnd1; i++) {
            offsprings[0].setGene(i, ind1.getGene(i));
            offsprings[1].setGene(i, ind2.getGene(i));
        }
        for (i=rnd1; i<rnd2; i++) {
            offsprings[0].setGene(i, ind2.getGene(i));
            offsprings[1].setGene(i, ind1.getGene(i));
        }
        for(i=rnd2; i<individualSize;i++){
            offsprings[0].setGene(i, ind1.getGene(i));
            offsprings[1].setGene(i, ind2.getGene(i));
        }
        
        offsprings[0].setChromosomeCovering();
        offsprings[0].addNecessaryRowsToNServices();
        offsprings[0].setMutationrate(ind1.getMutate().getMutationRate());
        offsprings[1].setChromosomeCovering();
        offsprings[1].addNecessaryRowsToNServices();
        offsprings[1].setMutationrate(ind1.getMutate().getMutationRate());
        
        return offsprings;
    }
    
    public void swap(int rnd1, int rnd2){   
        //swapne dva integery bez pouziti pomocne promenne pomoci XORu - NOTE ^ znaci operaci XOR
        rnd1 = (rnd2 ^= (rnd1 ^= rnd2)) ^ rnd1;
    }
}
