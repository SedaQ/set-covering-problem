<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : xlst_transform.xsl
    Created on : 4. březen 2015, 17:51
    Author     : Seda
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <h2>
                <b><big>Algorithms </big> <sup> overview</sup></b>
            </h2>
            <body>  
                <table border="1" style="width:100%">
                    <tr bgcolor="#9acd32">
                        <th style = "text-align:left">algorithm</th>
                        <th style = "text-align:left">dMax</th>
                        <th style = "text-align:left">typeOfAlgorithm</th>
                        <th style = "text-align:left">fileName</th>
                        <th style = "text-align:left">numberOfServices</th>
                        <th style = "text-align:left">serviceNames</th>
                    </tr>
                    <xsl:for-each select="algorithms/algorithm">
                    <tr>
                        <td><xsl:value-of select="@id"/></td>
                        <td><xsl:value-of select="dMax"/></td>
                        <td><xsl:value-of select="typeOfAlgorithm"/></td>
                        <td><xsl:value-of select="fileName"/></td>
                        <td><xsl:value-of select="numberOfServices"/></td>
                        <td><xsl:value-of select="serviceNames"/></td>
                    </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
    
</xsl:stylesheet>
