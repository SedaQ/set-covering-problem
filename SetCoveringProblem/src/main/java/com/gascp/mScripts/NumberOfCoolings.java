package com.gascp.mScripts;

/**
 *
 * @author Seda
 */
public class NumberOfCoolings {
    public NumberOfCoolings(){}
    
    public static int getNumberOfCoolings(double initTemperature, double coolingRate){
        int numberOfRepetitions = 0;        
        double finalTemperature = 1.0;
        while(initTemperature > finalTemperature){
            initTemperature = initTemperature * coolingRate;
            numberOfRepetitions++;
        }
   
        return numberOfRepetitions;
    }
    
    public static double getSecondsFromNanoseconds(double number){
        return number * Math.pow(10,-9);
    }
    
    public static void main(String[] args){
        System.out.println("Pocet ochlazeni je: " + getNumberOfCoolings(10000.0, 0.999));
        System.out.println("Prevod z nanometru na sekundy pri zadanem parametru number je: " + getSecondsFromNanoseconds(862357927900.0));
    }
    
}
