package com.gascp.mScripts;

import com.gascp.sa.SimulatedAnnealing;

/**
 *
 * @author Seda
 */

public class CheckSAEquals {
    
    public CheckSAEquals(){}
    
    public static void main(String args[]){
        // čím menší f tím líp hodnoty jsou možné příklady hodnot během běhu aplikace
        int f = 100;
        int f0 = 120;
        double initTemperature = 100.0;
        System.out.println("Rovnice v simulovanem zihani dava: " + Math.exp((-(f0-f))/(initTemperature)));
        // takto by asi mel vzorec vypadat pro minimalizacni ulohu, aby daval hodnotu v intervalu (0-1)
        
        System.out.println("Rovnice simulovaneho zihani podle knihy Zelinka.." + Math.exp((-(f-f0))/(initTemperature)));
        // => v knize Zelinka maji u minimalizacni ulohy pro simulovane zihani chybu
    }
}
