package com.gascp.mScripts;

import java.lang.management.*;

/**
 *
 * @author Seda
 */

public class ThreadCpuTime {
    public ThreadCpuTime(){}
 
    /** Get CPU time in nanoseconds.
     * @return  */
    public long getCpuTime( ) {
        ThreadMXBean bean = ManagementFactory.getThreadMXBean( );
        return bean.isCurrentThreadCpuTimeSupported( ) ?
            bean.getCurrentThreadCpuTime( ) : 0L;
    }

    /** Get user time in nanoseconds.
     * @return  */
    public long getUserTime( ) {
        ThreadMXBean bean = ManagementFactory.getThreadMXBean( );
        return bean.isCurrentThreadCpuTimeSupported( ) ?
            bean.getCurrentThreadUserTime( ) : 0L;
    }

}
