package com.gascp.mScripts;

import com.gascp.matrix.ReadData;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;

/**
 *
 * @author Seda
 */

public class TransformData {
   
    private int[][] originMatrix;
    private Map<Integer, ArrayList<String>> treeMap;
    private Map<Integer, Integer[]> hashMap;
    private static Random rand = new Random();
    private String[] cost;

    public TransformData(){}   
   
    // vezme hodnoty jednotlivych sluzeb
    public void readcost(File f){
        int[] rowsColumns = new int[2];
        String[] rowsColumns3 = new String[2];
        BufferedReader br = null;
        String line = null;
        int suma = 0;
        this.treeMap = new TreeMap<Integer, ArrayList<String>>();
       
        try{
            br = new BufferedReader(new FileReader(f.getAbsolutePath()));
            String[] rowsColumns2 = br.readLine().split(" ");
            rowsColumns3 = Arrays.copyOfRange(rowsColumns2, 1, rowsColumns2.length);
            for(int i = 0; i<rowsColumns3.length;i++){
                rowsColumns[i] = Integer.parseInt(rowsColumns3[i]);
            }
            this.cost = new String[rowsColumns[1]];
           
            while(this.cost.length > suma){
                String lajnecka = br.readLine().trim();
                String[] mamut = lajnecka.split(" ");
                for(int i=0;i<mamut.length;i++){
                        // sem ukladam vsechny hodnoty vah
                        this.cost[suma+i]=mamut[i];
                }
                suma += mamut.length;
            }
           
            System.out.println("Vahy jednotlivych sluzeb: " + Arrays.toString(cost));
            System.out.println();
           
            int radek =-1;
            String number = null;
            while((number = br.readLine().trim()) != null){
                int sum = 0;
                radek++;
                ArrayList<String> list = new ArrayList<String>();
                //System.out.println("Pocet pokrytych sloupcu v " + radek + " radku je: " + number);
   
                while(Integer.parseInt(number)>sum){
                    String lines = br.readLine().trim();
                    String[] matoha = lines.split(" ");
                    sum +=matoha.length;
                    for(int i=0;i<matoha.length;i++){
                        list.add(matoha[i]);   
                    }
                    //System.out.println("Suma je: " + sum);
                }
                //System.out.println("Hodnota number je: " + number);
                //System.out.print("A sloupce, ktere obsahuje: ");
                for(int i = 0; i < list.size(); i++) {  
                    System.out.print(list.get(i) + " ");
                }
               
                //System.out.println();
                //System.out.println();
                this.treeMap.put(radek,list);
                if(radek == rowsColumns[0]-1){ // nevim proc, ale standardni cteni souboru mi hazelo nullpointer exception
                    break;
                }
            }
       
            // vytvori dvourozmerne pole pozadovanych rozmeru
            createMatrix(rowsColumns[0], rowsColumns[1]);
            System.out.println("Pocet radku matice je: " + rowsColumns[0]);
            System.out.println("Pocet sloupcu matice je: " + rowsColumns[1]);
            //createMap(rowsColumns[0], rowsColumns[1]);

            // pokryje mapu
            fillOriginMatrix();
           
            for (Entry<Integer, ArrayList<String>> entry : treeMap.entrySet()){
                //System.out.println(entry.getKey() + "/" + entry.getValue());
            }

            //while((line = br.readLine()) !=null ){
            //    cost = line.split(" ");
            //}
           
            System.out.println(Arrays.toString(rowsColumns));
            //System.out.println(Arrays.toString(cost));
            System.out.println("Velikost souboru s cenami je: " + this.cost.length);
           
        } catch(IOException io){
            io.printStackTrace();
        } finally{
            if(br!=null){
                try{
                    br.close();
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        }
    }
   
    public void createMatrix(int rows, int columns){
        this.originMatrix = new int[rows][columns];
    }
   
    public void createMap(int rows, int columns){
        this.hashMap = new HashMap<Integer, Integer[]>();
        for(int i=0; i<rows;i++){
            this.hashMap.put(i, new Integer[columns]);
            System.out.println("radek " + i +" je:"+ Arrays.toString(hashMap.get(i)));
        }
    }
   
    public void fillOriginMatrix(){
        int i = 0;
        for(Entry<Integer, ArrayList<String>> entry : treeMap.entrySet()){
            i = entry.getKey();
            System.out.print("Key mapy cili radek je " + i + " a dava hodnoty: ");
            ArrayList<String> value = entry.getValue();
            //System.out.println("List a jeho velikost je: " + value.size());
            int s = 0;
            for(int j = 0; j<this.originMatrix[0].length;j++){
                //System.out.println("index, ktery se bere z listu je: " + Integer.parseInt(value.get(s)));
                if(j == Integer.parseInt(value.get(s))-1){
                    //System.out.println("index, do ktereho se vklada z listu je: " + j);
                    this.originMatrix[i][j] = 1;
                    // pokud je jsem na konci arraylistu, tka uz nezvetsuju index, ktery z nej vytahuji jinak by to hazelo IndexOutofBounds
                    if(!(s==value.size()-1)){
                        s++;
                    }
                }
            }
            System.out.println(Arrays.toString(this.originMatrix[i]));
        }
    }

    // metoda, vezme starej soubor, kterej obsahuje 1cky a nuly a zapise do noveho souboru podle dMAX nahodne cisla
    // tahle metoda slouzi zejmena pro tvorbu dat k testovani nikoliv pro beh samotne aplikace
    public void transformMatrix2AndReadToFile(File originFile, File newFile, int dMAX){
        StringBuilder sb = new StringBuilder();
        FileWriter fw = null;
        int[][] matrix = this.originMatrix;
        int rows = matrix.length;
        int columns = matrix[0].length;
        int[][] transformedMatrix = new int[rows][columns];
        for(int i=0;i<rows;i++){
            for(int j=0;j<columns;j++){
                if(matrix[i][j] == 1){
                    transformedMatrix[i][j] =  randInt(0,dMAX);
                    sb.append(transformedMatrix[i][j]).append(";");
                    //System.out.print(1 + " ");
                } else if(matrix[i][j] == 0){
                    transformedMatrix[i][j] = randInt(dMAX,dMAX+100);
                    sb.append(transformedMatrix[i][j]).append(";");
                    //System.out.print(0+ " ");
                }
            }
            sb.append(ReadData.EOLN);
        }
        try {
            fw = new FileWriter(newFile.getAbsolutePath());
            fw.write(sb.toString());
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if(fw != null){
                try {
                    fw.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
       
    public static int randInt(int min, int max) {

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
   
    
    public static void main(String[] args){
        TransformData rc = new TransformData();
        File f = new File("C:\\GitRepositories\\SCP\\SetCoveringProblem\\src\\main\\java\\dataFiles\\scpe5.txt");
        File newFile = new File("C:\\GitRepositories\\SCP\\SetCoveringProblem\\src\\main\\java\\transformedDataFiles\\scpe5DMax51.csv");
        rc.readcost(f);
        rc.transformMatrix2AndReadToFile(f, newFile, 52);
    }
    
}

