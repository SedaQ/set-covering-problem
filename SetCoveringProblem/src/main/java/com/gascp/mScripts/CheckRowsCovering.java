package com.gascp.mScripts;

import com.gascp.matrix.ReadData;
import com.gascp.sa.SimulatedAnnealing;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * This class was made for check covering of resulting rows, because i found some weird results
 * 
 * @author Seda
 */

public class CheckRowsCovering {
    
    private List<Integer> removedRows = new ArrayList<>();
    private int[][] matrix;
    
    public CheckRowsCovering(){}

    public int[][] getDataIntoMatrix(File file, String delimiter){
        BufferedReader r = null;
        String line = null;
        List<String[]> list = new ArrayList<>();
        int[][] vysl = null;

        try {
            r = new BufferedReader(new FileReader(file.getAbsolutePath()));
            while((line = r.readLine()) != null){
                if(!line.startsWith("#")){
                    String[] cols = line.split(delimiter);
                    list.add(cols); 
                }
            }
            int rows = list.size();
            int columns = list.get(0).length;
            vysl = new int[rows][columns];

            try {
                // put values from ArrayList<String[]> to two dimensional array String[][]
                for(int i = 0; i<rows;i++){
                    for(int j = 0; j<columns; j++){
                        try{
                        vysl[i][j] = Integer.parseInt(list.get(i)[j].trim());
                        } catch(NumberFormatException ex){
                            int pic;
                            pic = JOptionPane.ERROR_MESSAGE;
                            JOptionPane.showMessageDialog(null,"Input data dont contains only numbers with delimiters","",pic);
                            return null;
                        }
                    }
                }
            } catch(ArrayIndexOutOfBoundsException ex){
                int pic;
                pic = JOptionPane.ERROR_MESSAGE;
                JOptionPane.showMessageDialog(null,"Input data doesnt match CSV rules (same number of rows and columns","",pic);
                return null;
            } catch(Exception ex){
                System.out.println("Data maji spatny format" + ex);
                return null;
            }
            
            //System.out.println("Original data: " + Arrays.deepToString(vysl));
        } catch(IOException io ){
            io.printStackTrace();
        } catch(Exception ex){
            ex.printStackTrace();
        } finally {
            if(r !=null){
                try {
                    r.close();
                } catch (IOException ex) {
                    Logger.getLogger(ReadData.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return vysl;
    }    

    public int[][] reachAbilityMatrix(File file, String delimiter,int dMax){
        int[][] transformedMatrix = getDataIntoMatrix(file, delimiter);
        int suma = 0;
        
        for(int i=0; i<transformedMatrix.length;i++){
            for(int j=0; j<transformedMatrix[0].length;j++){
                if(transformedMatrix[i][j] > dMax){
                    transformedMatrix[i][j] = 0;
                } else {
                    transformedMatrix[i][j] = 1;
                    suma +=1;
                }       
            }
            suma=0;
        }
        this.matrix = transformedMatrix;
        
        return transformedMatrix;
    } 
    
    /**
     * method to return specific row depends on parametr radek from first input parametr matrix
     * @param matrix input matrix
     * @param radek input row which should be returned
     * @return 
     */
    public int[] getRowFromMatrix(int[][] matrix, Integer radek){
        // pocet radku
        int row = matrix.length;     
        // pocet kolonek
        int columns = matrix[0].length;
        int rows[] = new int[columns];
        for(int i=0;i<columns;i++){
            if(radek>row || radek<0){
                //System.out.println("V metode getRowFromMatrix, ktera je ve tride Matrix, chci vratit radek matice, ktery je vetsi nez pocet radku matice nebo naopak mensi nez pocet radku matice");
            } else { 
                rows[i] = matrix[radek][i];
            }
        }
        
        return rows;
    }
    
    /**
     * method to remove specific row from matrix
     * @param array array in which some specific row will be removed
     * @param REMOVE_ROWS
     * @param row specific row to remove
     * @return new array without specific row
     */
    public int[][] removeSpecificRowFromMatrix(int[][] array, List<Integer> REMOVE_ROWS){
        int[][] returnArray = new int[array.length-REMOVE_ROWS.size()][array[0].length];
        int m = 0;
        for(int i = 0; i<array.length;i++){
            if(REMOVE_ROWS.contains(i)){
                continue;
            }
            int n=0;
            for(int j =0; j<array[0].length;j++){
                returnArray[m][n] = array[i][j];
                ++n;
            }           
            ++m;
        }
        return returnArray;
    }    

    public int[] check(File f1,int dMax,List<Integer> list,String delimiter){
        int[][] matrix = reachAbilityMatrix(f1,delimiter, dMax);
        //System.out.println(Arrays.deepToString(matrix));
        int[] covering = new int[matrix[0].length]; 
        for(int i =0; i<matrix.length;i++){
            if(list.contains(i)){
                System.out.println("list obsahuje neco");
                for(int s = 0; s<matrix[0].length; s++){
                    if(matrix[i][s]==1){
                        covering[s]=1;
                    }
                }
            }
        }
        //System.out.println(Arrays.toString(covering));
        return covering;
    }
    
    public static boolean allAreTrue(int[] array){
        for(int i = 0;i<array.length;i++){
            if(array[i] == 0){
                return false;
            }
        }
        return true;
    }
    
    /*
    public void repairOperator() {
        File f1= new File("C:\\GitRepositories\\SCP\\SetCoveringProblem\\src\\main\\java\\transformedDataFiles\\scp41DMax52.csv");
        int dMax = 52;
        
        int[] row = new int[matrix[0].length];
        int[] neighbour;
        for(int asd=0;asd<matrix.length;asd++){
            if()
        }
        int[] neighbourCovering;
        for(int i=0; i<neighbour.length;i++){
            if(this.neighbour[i] == 0 && !this.necessaryServices.contains(this.unNecessaryRows.get(i))){
                reverseValue(this.neighbour[i]);
                row = getRowFromMatrix(this.matrix, matrix[0][i]);
                for(int k=0;k < row.length;k++){
                    if(row[k]==1){
                        if(this.neighbourCovering[k] == 1){
                            this.surplusIndividual++;
                        } else {
                            this.neighbourCovering[k] = 1;
                        }
                    }
                }
                this.necessaryServices.add(unNecessaryRows.get(i));
            }
            if(allAreTrue(neighbourCovering)){
                break;
            }
        }
    }
    */

    public static void main(String[] args){
        //File f1= new File("C:\\GitRepositories\\SCP\\SetCoveringProblem\\src\\main\\java\\transformedDataFiles\\scp41DMax52.csv");
        //File f1 = new File("C:\\GitRepositories\\SCP\\SetCoveringProblem\\src\\main\\java\\transformedDataFiles\\testData2");
        File f1 = new File("C:\\GitRepositories\\SCP\\SetCoveringProblem\\src\\main\\java\\transformedDataFiles\\scpe4DMax68.csv");
        
        int dMax = 52;
        int[] arrayOfResults = new int[]{2, 17, 21, 25};
        List<Integer> list = new ArrayList<>();
        for(int i=0;i<arrayOfResults.length;i++){
            list.add(arrayOfResults[i]);
        }
        
        int[] array = new int[]{0,0,0,0,0,1};
        int[] arrayTransformed = new int[array.length];
        for(int i=0;i<array.length;i++){
            if(array[i]==0){
                arrayTransformed[i] = TransformData.randInt(52,100);
            }else{
                arrayTransformed[i] = TransformData.randInt(0,52);
            }
        }
        System.out.println("Puvodni array" + Arrays.toString(array));
        System.out.println("Pretransformovane array " + Arrays.toString(arrayTransformed));
        
        CheckRowsCovering ch = new CheckRowsCovering();
        System.out.println("Pokryti chromozomu je: " + Arrays.toString(ch.check(f1,dMax,list,";")));
        System.out.println("Jsou s temito body pokryti vsichni zakaznici?:  " + allAreTrue(ch.check(f1,dMax,list,";")));
    }
    
}
