package com.gascp.sa;

import com.gascp.matrix.Matrix;

/**
 *
 * @author Seda
 */

public class NeighbourHood {
    
    private Neighbour[] neighboorhood;
    private Matrix matrix;
    //private int totalFitness;
    
    public NeighbourHood(Matrix matrix, int[] currentOccupant){
        this.matrix = matrix;
        neighboorhood = new Neighbour[currentOccupant.length];
        for(int i=0; i<matrix.getUnNecessaryRows().size();i++){
        //for(int i=0; i<10;i++){
            neighboorhood[i] = new Neighbour(matrix);
            neighboorhood[i].generateNeighbour(currentOccupant, i);
            //neighboorhood[i].generateNeighbourOptimalized(currentOccupant, Neighbour.randInt(0,currentOccupant.length));
            neighboorhood[i].setChromosomeCovering();
            neighboorhood[i].addNecessaryRowsToNServices();
            neighboorhood[i].evaluateIndividual();
            //totalFitness += neighboorhood[i].getFitnessValue();
        }
    }

    public Neighbour[] getNeighboorhood() {
        return neighboorhood;
    }
}
