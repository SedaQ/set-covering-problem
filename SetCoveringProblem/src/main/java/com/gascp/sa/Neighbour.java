package com.gascp.sa;

import com.gascp.matrix.IChromosomeRepair;
import com.gascp.matrix.Matrix;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Seda
 */

public class Neighbour implements IChromosomeRepair {
    private int neighbour[];
    private static Random rnd = new Random();
    private List<Integer> unNecessaryRows;
    private List<Integer> necessaryRows;
    private int[] neighbourCovering;
    private int[] originCovering;
    private List<Integer> necessaryServices;
    private int matrixColumnsLength;
    private int fitnessValue;
    private int surplusIndividual;
    private Matrix matrix;
    
    public Neighbour(Matrix matrix){
        this.matrix = matrix;
        fitnessValue = 0;
        neighbour = new int[matrix.getUnNecessaryRows().size()];
        neighbourCovering = new int[matrix.getMatrixColumnsLength()];
        necessaryServices = new ArrayList<>();
        originCovering = matrix.getUNContainsColumns();
        //System.out.println("Nezbytne radky pokryvaji tyto zakazniky:  "+Arrays.toString(originCovering));
        unNecessaryRows = matrix.getUnNecessaryRows();
        necessaryRows = matrix.getNecessaryRows();
        matrixColumnsLength = matrix.getMatrixColumnsLength();
        surplusIndividual = matrix.getSurplus();
    }
    
    public void generateNeighbour(int[] currentOccupant, int i){
        for(int j=0;j<currentOccupant.length;j++){
            if(i != j)
                this.neighbour[j] = currentOccupant[j];
            else
                this.neighbour[j] = reverseValue(currentOccupant[j]);            
        }
    }
    
    public void generateNeighbourOptimalized(int[] currentOccupant, int i){
        System.arraycopy(currentOccupant, 0, this.neighbour, 0, currentOccupant.length);
        this.neighbour[i] = reverseValue(currentOccupant[i]);
    }
    
    public int reverseValue(int i){
        return i ^=1;
    }
    
    /**
     * saved into field - chromosomeCovering which customers this solution (chromosome) covers
     */
    public void setChromosomeCovering(){
        setOriginCovering();
        //int[] rows = new int[SIZE+necessaryRows.size()];
        int[] row = new int[this.matrixColumnsLength];
        for(int i=0; i<this.neighbour.length;i++){
            if(this.neighbour[i]==1){
                row = matrix.getRowFromMatrix(this.matrix.getMatrix(), this.unNecessaryRows.get(i));
                for(int k=0;k<row.length;k++){
                    if(row[k]==1){
                        if(this.neighbourCovering[k]==1){
                            this.surplusIndividual++;
                        } else{
                            this.neighbourCovering[k]=1;
                        }
                    }
                }              
                // uklada pozice radku, ktere jsou nezbytne pro pokryti danych zakazniku
                this.necessaryServices.add(this.unNecessaryRows.get(i));
            }
        }
        // pokud nebyli timto resenim pokryti vsichni zakaznici, tak provadi opravu, do te doby nez se pokryji vsichni zakaznici
        if(!allAreTrue(this.neighbourCovering)){
            repairOperator();        
        }
    }
    
    /**
     * sets covering from necessary rows to chromosomeCovering
     */
    public void setOriginCovering(){
        for(int i = 0;i<this.neighbourCovering.length;i++){
            if(this.originCovering[i]==1){
                this.neighbourCovering[i]=1;
            } else {
                this.neighbourCovering[i]=0;
            }
        }
    }

    @Override
    public void repairOperator() {
        if(!allAreTrue(this.neighbourCovering)){
            int[] row = new int[this.matrixColumnsLength];
            for(int i=0; i<this.neighbour.length;i++){
                if(this.neighbour[i] == 0 && !this.necessaryServices.contains(this.unNecessaryRows.get(i))){
                    reverseValue(this.neighbour[i]);
                    row = this.matrix.getRowFromMatrix(this.matrix.getMatrix(), this.unNecessaryRows.get(i));
                    for(int k=0;k < row.length;k++){
                        if(row[k]==1){
                            if(this.neighbourCovering[k] == 1){
                                this.surplusIndividual++;
                            } else {
                                this.neighbourCovering[k] = 1;
                            }
                        }
                    }
                    this.necessaryServices.add(unNecessaryRows.get(i));
                }
                if(allAreTrue(this.neighbourCovering)){
                    break;
                }
            }
        }
    }  
    
    @Override
    public boolean allAreTrue(int[] array){
        for(int i = 0;i<array.length;i++){
            if(array[i] == 0){
                return false;
            }
        }
        return true;
    }
    
    public void addNecessaryRowsToNServices(){
        for(Integer list : this.necessaryRows){
            this.necessaryServices.add(list);
        }        
    }

    public void evaluateIndividual(){
        fitnessValue = 0;
        for(int i=0; i<necessaryServices.size(); i++){        
            this.fitnessValue += matrix.getCost()[necessaryServices.get(i)];
        }
    }
    
    /**
         * generates values of chromosome for first currentOccupant
    */
    public void generateNeighbour(){
        for(int i=0;i<this.neighbour.length;i++){
            this.neighbour[i] = randInt(0,1);
            //System.out.println("Hodnota v obyvateli je:" + Arrays.toString(neighbour));
        }
    }

    public int getFitnessValue() {
        return fitnessValue;
    }

    public int[] getNeighbour() {
        return neighbour;
    }

    public List<Integer> getNecessaryRows() {
        return necessaryRows;
    }

    public List<Integer> getNecessaryServices() {
        return necessaryServices;
    }

    public int[] getNeighbourCovering() {
        return neighbourCovering;
    }
    
    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }
    
}
