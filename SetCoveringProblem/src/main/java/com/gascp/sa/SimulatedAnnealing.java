package com.gascp.sa;

import com.gascp.mScripts.ThreadCpuTime;
import com.gascp.matrix.IAlgorithm;
import com.gascp.matrix.Matrix;
import com.gascp.parsers.dom.DomWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JProgressBar;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

/**
 *
 * @author Seda
 */

public class SimulatedAnnealing implements IAlgorithm, Cloneable{    
 
    private final Matrix matrix;
    private static Random rnd = new Random();
    private Neighbour currentOccupant;
    private ArrayList<Integer> bestFitnessList;
    private ArrayList<Neighbour> lOfOccup;
    private Map<Integer, List<Neighbour>> lOfListsOccupants;
    private JProgressBar progressBar;
    private List<Integer> currentOccupantList; 
    private List<List<Integer>> best;
    private ThreadCpuTime timeManager;
    private int repetition;
    private DomWriter write;
    private File filePathXML;
    private String typeOfAlgorithm;
    private String fileName;
    private String[] serviceNames;
    private int DMax;
    private int order;
    //private NeighbourHood neighbours;
    
    
    public SimulatedAnnealing(Matrix matrix, String fileName, int DMax, JProgressBar progressBar, int repetition){
        this.matrix = matrix;
        this.DMax = DMax;
        this.progressBar = progressBar;
        this.fileName = fileName;
        this.filePathXML = new File("./src/main/java/com/mendelu/ga_scp/dataStorageXML/DataStorage.xml");
        this.typeOfAlgorithm = "Simulated Annealing";
        timeManager = new ThreadCpuTime();
        this.repetition = repetition;
        best = new ArrayList<>();
        lOfOccup = new ArrayList<>();
        lOfListsOccupants = new HashMap<Integer, List<Neighbour>>();
        order = 0;
        write = new DomWriter();
    }
    
    //C:\Users\seda\Documents\NetBeansProjects\SCP\SetCoveringProblem\src\main\java\transformedDataFiles
    @Override
    public void startAlgorithm(){
        double initTemperature = 10000.0;
        int finalTemperature = 1;
        double coolingRate = 0.999;
        long startCpuTime = this.timeManager.getCpuTime();
                
        int nT = 1;
        int value = 0;
        int size = this.matrix.getUnNecessaryRows().size();
        
        for(int n=0; n<this.repetition; n++){
            this.bestFitnessList = new ArrayList<>();
            this.bestFitnessList.add(this.matrix.getMatrixRowsLength());
            this.currentOccupant = generateCurrentOccupant();
            this.bestFitnessList.add(this.currentOccupant.getFitnessValue());
            this.lOfOccup = new ArrayList<>();
            this.lOfOccup.add(this.currentOccupant);
            //this.currentOccupant.repairOperator();
            NeighbourHood neighbours = new NeighbourHood(this.matrix, this.currentOccupant.getNeighbour());
            
            while(initTemperature > finalTemperature){
                for(int i=0; i<nT; i++){
                    Neighbour neighbour = neighbours.getNeighboorhood()[this.rnd.nextInt(size)];
                    //neighbour.repairOperator();
                    int f = this.currentOccupant.getFitnessValue();
                    int f0 = neighbour.getFitnessValue();
                    if(f > f0){ // když je soused lepší než současný obyvatel (currentOccupant ma vetsi hodnotu, coz vzhledem k minimalizacni uloze znaci, ze je horsi)
                        this.currentOccupant = neighbour;
                        neighbours = new NeighbourHood(this.matrix, this.currentOccupant.getNeighbour());
                    }
                    else {
                        double r = this.rnd.nextDouble();
                        if(r < Math.exp((f-f0)/(initTemperature/15))){
                            this.currentOccupant = neighbour;
                            neighbours = new NeighbourHood(this.matrix, this.currentOccupant.getNeighbour());
                        }
                    }
                }
                this.progressBar.setValue(++value);
                this.bestFitnessList.add(this.currentOccupant.getFitnessValue());
                this.lOfOccup.add(this.currentOccupant);
                initTemperature = initTemperature*coolingRate;
            }
            initTemperature = 10000.0;
            System.out.println("Vysledne pole: " + getBestFitnessList());
            this.best.add((List<Integer>) this.bestFitnessList.clone());
            this.lOfListsOccupants.put(n, (List<Neighbour>) this.lOfOccup.clone());
        } 
        setBestFitListFRepet();
        System.out.println("Vysledne pole ze všech opakování: " + getBestFitnessList());
        long finishCpuTime = timeManager.getCpuTime();
        long runTaskTime = finishCpuTime - startCpuTime;
        
        System.out.println("Začátek běhu algoritmu byl v čase : " + startCpuTime);
        System.out.println("Konec běhu algoritmu byl v čase : " + finishCpuTime);
        System.out.println("Čas běhu algoritmu byl: " + runTaskTime);
        Neighbour bestState = this.lOfListsOccupants.get(order).get(this.lOfListsOccupants.get(order).size()-1);
        saveServiceNamesFromBestState(bestState);
        //writeToXML(bestState);
        Path results = Paths.get("./src/main/resources/results/sa-results-" + System.currentTimeMillis() + ".csv");
        try {
            Files.createFile(results);
            Files.write(results, new StringBuilder("#id,best-individual").append(System.lineSeparator()).toString().getBytes(), StandardOpenOption.CREATE);
            StringBuilder sb = new StringBuilder();
            for(int i=0; i<bestFitnessList.size();i++){
                sb.append(i).append(",").append(bestFitnessList.get(i)).append(System.lineSeparator());
            }
            Files.write(results, sb.toString().getBytes("UTF-8"), StandardOpenOption.APPEND);
        } catch (IOException ex) {
            Logger.getLogger(SimulatedAnnealing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void saveServiceNamesFromBestState(Neighbour bestState){
        this.serviceNames = new String[bestState.getNecessaryServices().size()];
        serviceNamesArray(bestState.getNecessaryServices());
    }
    
    public void serviceNamesArray(List<Integer> list){
        //convert List<Integer> to String[]
        for(int i = 0; i<list.size(); i++){
            if(i==list.size()-1){
                this.serviceNames[i] = String.valueOf(list.get(i));
            } else{
                this.serviceNames[i] = String.valueOf(list.get(i)+",");
            }
        }
    }    
    
    public void setBestFitListFRepet(){
        List<List<Integer>> list = getBest();
        int max = Integer.MAX_VALUE;
        int lastMember = list.get(0).size()-1;
        for(int i=0;i<list.size();i++){
            if(max > list.get(i).get(lastMember)){
                max = list.get(i).get(lastMember);
                this.order = i;
            }
        }
        this.bestFitnessList = new ArrayList<>(list.get(this.order));
    } 
   
    public void writeToXML(Neighbour bestState){
        try {
            this.write.write(this.filePathXML, String.valueOf(this.DMax), this.typeOfAlgorithm, this.fileName, String.valueOf(bestState.getNecessaryServices().size()), this.serviceNames);
        } catch (TransformerException | ParserConfigurationException | SAXException | IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public static int randInt(int min, int max) {

        int randomNum = rnd.nextInt((max - min) + 1) + min;

        return randomNum;
    }
    
    public Neighbour generateCurrentOccupant(){
        Neighbour occupant = new Neighbour(this.matrix);
        occupant.generateNeighbour();
        occupant.setChromosomeCovering();
        occupant.addNecessaryRowsToNServices();
        occupant.evaluateIndividual();
        return occupant;
    }
    
    public static int getNumberOfCoolings(){
        int numberOfRepetitions = 0;
        double initTemperature = 1000.0;
        double finalTemperature = 1.0;
        double coolingRate = 0.99;
        
        while(initTemperature > finalTemperature){
            initTemperature = initTemperature * coolingRate;
            numberOfRepetitions++;
        }
        
        return numberOfRepetitions;
    }
    
    public List<List<Integer>> getBest(){
        return this.best;
    }

    @Override
    public List<Double> getAverageFitness() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<Integer> getBestFitnessList() {
        return bestFitnessList;
    }

    public List<Integer> getCurrentOccupantList() {
        return currentOccupantList;
    }
    
/*
    public void startAlgorithm(){
        double initTemperature = 1000.0;
        double finalTemperature = 1.0;
        double coolingRate = 0.999;        
        this.currentOccupant = generateCurrentOccupant();
        NeighbourHood neighbours = new NeighbourHood(this.matrix, this.currentOccupant.getNeighbour());               
        int nT = 1;        
        while(initTemperature > finalTemperature){
            for(int i=0; i<nT; i++){
                Neighbour neighbour = neighbours.getNeighboorhood()[this.rnd.nextInt(this.matrix.getUnNecessaryRows().size())];
                int f = this.currentOccupant.getFitnessValue();
                int f0 = neighbour.getFitnessValue();
                if(f > f0){ // když je soused lepší než současný obyvatel
                    this.currentOccupant = neighbour;
                    neighbours = new NeighbourHood(this.matrix, this.currentOccupant.getNeighbour());
                }
                else {
                    double r = this.rnd.nextDouble();
                    System.out.println("Hodnota rovnice je: " + (Math.exp(-(f-f0)/initTemperature)));
                    System.out.println("Hodnota cisla r je: " + r);
                    if(r < Math.exp(-((f-f0)/initTemperature))){
                        this.currentOccupant = neighbour;
                        neighbours = new NeighbourHood(this.matrix, this.currentOccupant.getNeighbour());
                    }
                }
            }
            initTemperature = initTemperature * coolingRate;
        }
    }
   */ 
}