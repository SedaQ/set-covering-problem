package com.gascp.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Seda
 */

public class DBConnection {
    
    private static final String DRIVER = "oracle.jdbc.driver.OracleDriver";
    private static final String HOST = "jdbc:oracle:thin:@host:1521:database";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    
    public DBConnection(){    
    }
    
    /**
     * load driver for connection to database
     */
    public static void loadDriver(){
        try {
            Class.forName(DRIVER);
        } catch(ClassNotFoundException ex){
            System.out.println("Load driver fail: " + ex);
        }
    }
    
    /**
     * 
     * @return connection to database it tries 3 times in case that connection failed
     */
    public static Connection getConnection(){
        int attempts = 0;
        Connection conn = null;
        boolean result = false;
        while(!result && attempts < 3){
            try {
                conn = DriverManager.getConnection(HOST,USERNAME,PASSWORD);
                result = true;
            } catch (SQLException ex) {
                System.out.println("Program se nemohl připojit k databází: " + ex);
                attempts++;
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException exc) {
                    System.out.println("Nepovedlo se chvilkove uspani threadu pri connection do database: " + exc);
                }
            }
        }
        return conn;
    }
    
    /**
     * 
     * @param con close connection to database
     */
    public static void closeConnection(Connection con){
        int attemps = 0;
        boolean result = false;
        while(!result && attemps < 3){
            try {
                con.close();
                result = true;
            } catch (SQLException ex) {
                attemps++;
                System.out.println("Nepodarilo se zavrit connection do databaze: " + ex);
            }
        }
    }
}
