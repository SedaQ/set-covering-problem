package com.gascp.ga.selection;

import com.gascp.population.Individual;
import com.gascp.population.Population;
import java.util.List;

/**
 *
 * @author Seda
 */
public interface ISelection {
    
    public List<Individual> selection(Population pop);
    
}
