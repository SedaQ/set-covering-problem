package com.gascp.ga.selection;

import com.gascp.population.Individual;
import com.gascp.population.Population;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Seda
 */

public class TournamentSelection implements ISelection {
    
    public TournamentSelection(){}

    @Override
    public List<Individual> selection(Population pop) {
        int best = Integer.MAX_VALUE;
        Individual bestIndividual = null;
        int velikost = pop.getPopulation().size()-1;
        int tournamentPoolSize = (velikost+1)/10;
        
        List<Individual> winners = new ArrayList<>();
        List<Individual> picked = new ArrayList<>();
        
        for(int i = 0; i<tournamentPoolSize; i++){
            picked.add(pop.getPopulation().get(RouletteWheelSelection.randInt(0, velikost)));
        }

        for (Individual p : picked) {
            if(p.getFitnessValue() < best){
                best = p.getFitnessValue();
                bestIndividual = p;
            }
        }
        
        winners.add(bestIndividual);
        
        picked.clear();
        
        for(int i = 0; i<tournamentPoolSize; i++){
            picked.add(pop.getPopulation().get(RouletteWheelSelection.randInt(0, velikost)));
        }
        
        best = Integer.MAX_VALUE;
        bestIndividual = null;
        for (Individual p : picked) {
            if(p.getFitnessValue() < best){
                best = p.getFitnessValue();
                bestIndividual = p;
            }
        }
        winners.add(bestIndividual);
        
        return winners;
        /*
        // tohle by slo pro binarni turnajovej vyber
        int i = RouletteWheelSelection.randInt(0, velikost);
        int j;
        int i1;
        
        do
            j = RouletteWheelSelection.randInt(0, velikost);
        while(i != j);
        if(pop.getPopulation()[i].getFitnessValue() < pop.getPopulation()[j].getFitnessValue())
            winners.add(pop.getPopulation()[i]);
        else {
            i = j;
            winners.add(pop.getPopulation()[i]);
        }
        
        do
            i1 = RouletteWheelSelection.randInt(0, velikost);
        while(i1 != i);
        do
            j = RouletteWheelSelection.randInt(0, velikost);
        while(j != i && j!=i1);
        if(pop.getPopulation()[i1].getFitnessValue() < pop.getPopulation()[j].getFitnessValue())
            winners.add(pop.getPopulation()[i1]);
        else
            winners.add(pop.getPopulation()[j]);
        */
    }    
}
