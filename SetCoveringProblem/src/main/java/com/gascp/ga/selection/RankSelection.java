package com.gascp.ga.selection;

import com.gascp.population.Individual;
import com.gascp.population.Population;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Seda
 */
public class RankSelection implements ISelection {
    
    private RouletteWheelSelection r;
    
    public RankSelection(){
        r = new RouletteWheelSelection();
    }
       
    @Override
    public List<Individual> selection(Population pop) {
        Collections.sort(pop.getPopulation(), Population.IndividualFitnessComparator);
        
        List<Individual> vyselektovaniJedinci = new ArrayList();      
        // pomoci ruletove selekce vybere dva 
        vyselektovaniJedinci.add(this.r.rouletteForRankSelection(pop.getPopulation()));
        vyselektovaniJedinci.add(this.r.rouletteForRankSelection(pop.getPopulation()));
        
        //osetreni aby jedinci nebyli stejni
        if(vyselektovaniJedinci.get(0) == vyselektovaniJedinci.get(1)){
            while(vyselektovaniJedinci.get(0) != vyselektovaniJedinci.get(1)){
                vyselektovaniJedinci.remove(1);
                vyselektovaniJedinci.add(this.r.rouletteForRankSelection(pop.getPopulation()));
            }
        }
        
        return vyselektovaniJedinci;
    }
             
}
