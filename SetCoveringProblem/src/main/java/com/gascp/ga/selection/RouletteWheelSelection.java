package com.gascp.ga.selection;

import com.gascp.population.Individual;
import com.gascp.population.Population;
import com.gascp.matrix.IndividualComparator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Seda
 */

public class RouletteWheelSelection implements ISelection {
    
    public RouletteWheelSelection(){}

    @Override
    public List<Individual> selection(Population pop) {
       
        List<Individual> list = new ArrayList<>();
        list.addAll(pop.getPopulation());
        // seradim naclonovanou populaci tady populaci ascending
        sortIndividualsByFitnessValue(list);
        
        
        // seradim populaci descending
        Collections.sort(pop.getPopulation(), Population.IndividualFitnessComparator);
        
        int sumaFitness = 0;
        for (Individual population : pop.getPopulation()) {
            sumaFitness += population.getFitnessValue();
        }
        
        int[] poradi = new int[2];
        List<Individual> winners = new ArrayList<>();   
        while((winners.size()!=2)){
            int soucet =0;
            int rndNumber = randInt(0,sumaFitness);
            for(int i=0;i<Population.getPopulationSize();i++){
                soucet += pop.getPopulation().get(i).getFitnessValue();
                if(soucet>= rndNumber){
                    winners.add(list.get(i));
                    if(winners.size()==2 && winners.get(0)==winners.get(1)){
                        winners.remove(1);
                    }
                    break;
                }
            }
        }
        
        return winners;
    }    
    
    public Individual rouletteForRankSelection(List<Individual> ind){    
        // dle rank selection secte celkovou fitness, tak ze nejhorsi jedinec má fitness hodnotu 1 druhý nejhorší 2 atd.
        
        int suma = count(Population.getPopulationSize());
        
        // urci nahodne cislo z intervalu 0 az celkova fitness
        int rndNumber = randInt(0,suma);    
        int soucet = 0;
        
        //prochazi populaci a pocita sumu ucelovych funkci od hodnoty nula pokud se hodnota stane vetsi nebo rovnou rndNumber,
        //tak je vybran dany jedinec do reprodukcniho pole
        for(int i=0;i<Population.getPopulationSize();i++){
            soucet += i;
            if(soucet>= rndNumber){
                return ind.get(i);
            }
        }     
        
        // nemelo by sem nikdy dojit
        return null;
    }
    
    public int count(int limit){
        int suma = 0;
        for(int i=0;i<limit;i++){
            suma += i;
        }
        return suma;
    }

    public static int randInt(int min, int max) {

        int randomNum = Individual.getRnd().nextInt((max - min) + 1) + min;

        return randomNum;
    }
    
    /**
     * 
     * @param list sort input list ascending
     */
    public void sortIndividualsByFitnessValue(List<Individual> list){
        Collections.sort(list, new IndividualComparator()); 
    }
}
