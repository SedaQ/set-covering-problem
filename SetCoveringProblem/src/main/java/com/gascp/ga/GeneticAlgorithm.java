package com.gascp.ga;

import com.gascp.mScripts.ThreadPool;
import com.gascp.mScripts.ThreadCpuTime;
import com.gascp.population.Population;
import com.gascp.matrix.IndividualComparator;
import com.gascp.population.Individual;
import com.gascp.ga.selection.ISelection;
import com.gascp.ga.selection.RouletteWheelSelection;
import com.gascp.crossover.ICrossover;
import com.gascp.matrix.IAlgorithm;
import com.gascp.matrix.Matrix;
import com.gascp.parsers.dom.DomWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JProgressBar;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

/**
 *
 * @author Seda
 */

public class GeneticAlgorithm implements IAlgorithm {
    
    private int DMax;
    private String typeOfAlgorithm;
    private String fileName;
    private String[] serviceNames;
    private Population pop;
    private ISelection selection;
    private ICrossover crossover;
    private ThreadCpuTime timeManager;
    private int generations;
    private final Matrix matrix;
    private DomWriter write;
    private File filePathXML;
    private ArrayList<Double> averageFitnessList;
    private ArrayList<Integer> bestFitnessList;
    private JProgressBar progress;
    private double mutationRate;
    private int repetition;
    private ThreadPool pool;
    private List<List<Double>> avg;
    private List<List<Integer>> best;
    private int bestValue;
    
    public GeneticAlgorithm(Matrix matrix, int dMax, String fileName, ISelection selection, ICrossover crossover, int generations, JProgressBar progress, double mutationRate,int repetition){
        write = new DomWriter();
        filePathXML = new File("./src/main/java/com/gascp/dataStorageXML/DataStorage.xml");
        this.matrix = matrix;
        this.DMax = dMax;
        typeOfAlgorithm = "";
        this.fileName = fileName;
        this.selection = selection;
        this.crossover = crossover;
        this.generations = generations;
        typeOfAlgorithm = "GA steady-state";
        averageFitnessList = new ArrayList<>();
        bestFitnessList = new ArrayList<>();
        this.progress = progress;
        this.mutationRate = mutationRate;
        this.repetition = repetition;
        pool = new ThreadPool(10);
        avg = new ArrayList<>();
        best = new ArrayList<>();
        bestValue = Integer.MAX_VALUE;
        timeManager = new ThreadCpuTime();
    }
    
    @Override
    public void startAlgorithm(){
        try {
            int rankBest = 0;
            long startCpuTime = timeManager.getCpuTime();
            long suma = 0;
            
            double percentage = 30.0;
            
            List<Individual> inds = new ArrayList<>();
            
            for(int n=1; n<=this.repetition; n++){
                clearFieldsInNextRepetition(n);
                initPopAndFields();
                for(int i=0; i<this.generations;i++){
//                    if(i == this.generations/4){
//                        for(int ii=0;ii<this.pop.getPopulation().size()/2;ii++){
//                            inds.add(this.pop.getPopulation().get(ii));
//                        }
//                    }
//                    if(i == this.generations/3){
//                        duringWar(this.pop);
//                        warConsequences(inds);
//                        inds.clear();
//                        for(int ii=0;ii<this.pop.getPopulation().size()/2;ii++){
//                            inds.add(this.pop.getPopulation().get(ii));
//                        }
//                    }
//                    if(i == this.generations/2){
//                        duringWar(this.pop);
//                        warConsequences(inds);
//                    }

                    geneticOperations(i, n);
                }
                
                this.pop.printPopulation();
                List<Individual> bestIndividual = getBestIndividual();
                rankBest = manageBestRunSequence(rankBest, n, bestIndividual);
                saveServiceNamesFromBestInd(bestIndividual);
                writeToXML(bestIndividual);
            }
            
            Path results = Paths.get("./src/main/resources/results/without-war-elimination-" + System.currentTimeMillis() + ".csv");
            Files.write(results, new StringBuilder("#id,average-individual,best-individual").append(System.lineSeparator()).append("id,average-individual,best-individual").append(System.lineSeparator()).toString().getBytes(), StandardOpenOption.CREATE);
            StringBuilder sb = new StringBuilder();
            for(int i=0; i<averageFitnessList.size();i++){
                sb.append(i).append(",").append(Math.round(averageFitnessList.get(i))).append(",").append(bestFitnessList.get(i)).append(System.lineSeparator());
            }
            Files.write(results, sb.toString().getBytes("UTF-8"), StandardOpenOption.APPEND);
            
            long finishCpuTime = timeManager.getCpuTime();
            long runTaskTime = finishCpuTime - startCpuTime;
            Arrays.sort(this.serviceNames);
            System.out.println("Jmena nezbytnych sluzeb: " + Arrays.toString(this.serviceNames));
            System.out.println("Začátek běhu algoritmu byl v čase : " + startCpuTime);
            System.out.println("Konec běhu algoritmu byl v čase : " + finishCpuTime);
        } catch (IOException ex) {
            Logger.getLogger(GeneticAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void geneticOperations(int i, int n){
        List<Individual> ind = new ArrayList<>(this.selection.selection(this.pop));
        Individual[] offsprings = this.crossover.crossover(this.matrix, ind.get(0), ind.get(1));
        mutateOffsprings(offsprings);
        checkCovering(offsprings);
        evaluateOffsprings(offsprings);
        insertOffspringsToPopulation(this.pop, offsprings);
        addNodeToAvgAndBestFitnessList();
        this.progress.setValue(i+((n-1)*this.generations));
    }
        
    public void duringWar(Population pop){
        Collections.sort(pop.getPopulation(), new IndividualComparator());
        int populationSize = pop.getPopulation().size();
        
        int firstGroup = (int) (0.05 * populationSize); 
        int secondGroup = (int) (0.15 * populationSize);
        int thirdGroup = (int) (0.30 * populationSize);
        int fourthGroup = (int) (0.50 * populationSize);
        
        try {
            List<Individual> sortedPopulation = pop.getPopulation();
            int numberOfRemovedFromFirstGroup = (int) (firstGroup * 0.2);
            int numberOfRemovedFromSecondGroup = (int) (secondGroup * 0.4);
            int numberOfRemovedFromThirdGroup = (int) (thirdGroup * 0.5);
            int numberofRemovedFromFourthGroup = (int) (fourthGroup * 0.56);
            
            int fg = firstGroup;
            int sg = fg+secondGroup - numberOfRemovedFromFirstGroup;
            int tg = sg+thirdGroup - numberOfRemovedFromFirstGroup - numberOfRemovedFromSecondGroup;
            int fourthg = tg + fourthGroup - numberOfRemovedFromFirstGroup - numberOfRemovedFromSecondGroup - numberOfRemovedFromThirdGroup;
            
            System.out.println("Nejlepsi fitness je: " + pop.getBestFitness());
            sortedPopulation.subList(firstGroup-numberOfRemovedFromFirstGroup, fg).clear();
            System.out.println("Nejlepsi fitness je: " + pop.getBestFitness());
            sortedPopulation.subList(sg-numberOfRemovedFromSecondGroup, sg).clear();
            sortedPopulation.subList(tg-numberOfRemovedFromThirdGroup, tg).clear();
            sortedPopulation.subList(fourthg-numberofRemovedFromFourthGroup, fourthg).clear();
                        
            Population.setPOPULATION_SIZE(this.pop.getPopulation().size());
        } catch(Exception ex){
            //handle exp
            System.out.println(ex);
        }
    }
    
    private void warConsequences(List<Individual> inds) {
        Population.setPOPULATION_SIZE(this.pop.getPopulation().size()*2);
        this.pop.getPopulation().addAll(inds);
    }
    
    public void duringWarRemoveRandomlyEverySecond(Population pop){
        int indID = 0;
        for (Iterator<Individual> iter = this.pop.getPopulation().listIterator(); iter.hasNext(); ) {
            indID++;
            Individual ind = iter.next();
                if(indID % 2 == 0){
                    iter.remove();
                }
        }
        Population.setPOPULATION_SIZE(this.pop.getPopulation().size());
    }
    
    public void mutateOffsprings(Individual[] offsprings){
        offsprings[0].getMutate().mutation();
        offsprings[1].getMutate().mutation();
    }
    
    /**
     * in case that the solution didnt cover all customers 
     * @param offsprings
     */
   public void checkCovering(Individual[] offsprings){

        if(offsprings[0].allAreTrue(offsprings[0].getChromosomeCovering())){
            offsprings[0].repairOperator();
        }
        if(offsprings[1].allAreTrue(offsprings[1].getChromosomeCovering())){
            offsprings[1].repairOperator();
        }
    } 

    public void evaluateOffsprings(Individual[] offsprings){
        offsprings[0].evaluateIndividual();
        offsprings[1].evaluateIndividual();
    }
    
    public void insertOffspringsToPopulation(Population pop, Individual[] offsprings){
        // seradim populaci od nejhorsich k nejlepsim
        //sortIndividualsByFitnessValue(pop.getPopulation());
        Collections.sort(pop.getPopulation(), Population.IndividualFitnessComparator);
//        Arrays.sort(pop.getPopulation(), Population.IndividualFitnessComparator);
        
        //vyberu nahodneho jedince z podprumernych a zmenim jeho referenci na potomka1
        int random = RouletteWheelSelection.randInt(0, pop.getPopulation().size()/2);
        //pop.getPopulation()[random] = offsprings[0];
        pop.getPopulation().set(random, offsprings[0]);
       
        //vyberu nahodneho jedince z podprumernych a zmenim jeho referenci na potomka1
        int rnd = RouletteWheelSelection.randInt(0, pop.getPopulation().size()/2);
        pop.getPopulation().set(rnd, offsprings[1]);
    }
    
    public void saveServiceNamesFromBestInd(List<Individual> bestIndividual){
        this.serviceNames = new String[bestIndividual.get(0).getNecessaryServices().size()];
        serviceNamesArray(bestIndividual.get(0).getNecessaryServices());        
    }
    
    public void serviceNamesArray(List<Integer> list){
        //convert List<Integer> to String[]
        for(int i =0;i<list.size();i++){
            this.serviceNames[i] = String.valueOf(list.get(i));
        }
    }

    
    public void writeToXML(List<Individual> bestIndividual){
        try {
            this.write.write(this.filePathXML, String.valueOf(this.DMax), this.typeOfAlgorithm, this.fileName, String.valueOf(bestIndividual.get(0).getNecessaryServices().size()), this.serviceNames);
        } catch (TransformerException | ParserConfigurationException | SAXException | IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void addNodeToAvgAndBestFitnessList(){
        this.averageFitnessList.add(this.pop.calculateAverageFitness());
        this.bestFitnessList.add(this.pop.findBestFitness());
    }
    
    public void initPopAndFields(){
        this.bestFitnessList.add(this.matrix.initializeFitness());
        this.averageFitnessList.add((double)this.matrix.initializeFitness());
        //System.out.println("Pocatecni populace ma nejlepsiho jedince: " + this.matrix.initializeFitness());
        //System.out.println("Pocatecni populace ma prumernou hodnotu fitness: " + this.matrix.initializeFitness());
        this.pop = new Population(this.matrix);
        this.pop.evaluateIndividuals();
        this.pop.setMutationRate(this.mutationRate);
        this.averageFitnessList.add(this.pop.calculateAverageFitness());
        this.bestFitnessList.add(this.pop.findBestFitness());
    }
    
    public void clearFieldsInNextRepetition(int n){
        if(n!=1){
            this.bestFitnessList = null;
            this.averageFitnessList = null;
            this.pop = null;
            this.serviceNames = null;
            this.averageFitnessList = new ArrayList<>();
            this.bestFitnessList = new ArrayList<>();
        }
    }
    
    public List<Individual> getBestIndividual(){
            List<Individual> bestIndividual = new ArrayList<>();
            bestIndividual.add(this.pop.findBestIndividual());
            
            return bestIndividual;
    }

    public void sortIndividualsByFitnessValue(List<Individual> list){
        Collections.sort(list, new IndividualComparator()); 
    }
    
    
    public int manageBestRunSequence(int rankBest, int n,List<Individual> bestIndividual){
        if(this.bestValue < bestIndividual.get(0).getFitnessValue()){
                this.bestValue = bestIndividual.get(0).getFitnessValue();
                rankBest = n-1;
        }
        this.avg.add((ArrayList) this.averageFitnessList.clone());
        this.best.add((ArrayList) this.bestFitnessList.clone());
        if(n==this.repetition){
            this.averageFitnessList.clear();
            this.bestFitnessList.clear();
            this.averageFitnessList = new ArrayList<>(this.avg.get(rankBest));
            this.bestFitnessList = new ArrayList<>(this.best.get(rankBest));
        }
        return rankBest;
    }

    @Override
    public List<Double> getAverageFitness() {
        return averageFitnessList;
    }

    @Override
    public List<Integer> getBestFitnessList() {
        return bestFitnessList;
    }
        
    public void setdMax(int dMax) {
        this.DMax = dMax;
    }

    public void setTypeOfAlgorithm(String typeOfAlgorithm) {
        this.typeOfAlgorithm = typeOfAlgorithm;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public void setPop(Population pop) {
        this.pop = pop;
    }

    public void setSelection(ISelection selection) {
        this.selection = selection;
    }

    public void setCrossover(ICrossover crossover) {
        this.crossover = crossover;
    }

    public int getdMax() {
        return DMax;
    }

    public String getTypeOfAlgorithm() {
        return typeOfAlgorithm;
    }

    public String getFileName() {
        return fileName;
    }

    public Population getPop() {
        return pop;
    }

    public ISelection getSelection() {
        return selection;
    }

    public ICrossover getCrossover() {
        return crossover;
    }
    
}